# **DEPHISIT - [Servicio Android]** #

Este proyecto tiene como objetivo servir de ejemplo (en base a datos reales y/o simulados) sobre como pueden interactuar las aplicaciones Android con el servicio desarrollado por la UMA, para hacer uso de los dispositivos/sensores DEPHISIT del sistema.

La funcionalidad actual del servicio Android desarrollado consiste en:
1) Descubrir todos los dispositivos BLE presentes en el entorno.
2) Procesar todos los servicios, características y descriptores de estos dispositivos BLE.
3) Generar los correspondientes dispositivos virtuales DEPHISIT, en base a lo descubierto y procesado en los pasos anteriores, para que las aplicaciones cliente puedan usarlos.

El objetivo final de este servicio es ofrecer una API mediante la que cualquier aplicación de la plataforma Android pueda interactuar con los dispositivos del sistema DEPHISIT. Por ello se ha usado como base un servicio Android de tipo “Bound Service” que permite que las actividades de otras aplicaciones se conecten a él, envíen peticiones y obtengan respuestas. Sin embargo, por el momento el servicio (API) desarrollado no hace uso de mensajes (clases “Messenger” y “Handler”) que permite la comunicación entre distintos procesos.

*NOTA: Por lo tanto, todo lo expuesto a continuación puede verse afectado en el futuro si finalmente el servicio debe hacer uso de mensajes para la interacción con las aplicaciones cliente. De todas formas, la lógica de intercambio de mensajes sería similar a lo que existe en la actualidad.*

# Descripción de los principales componentes del proyecto #

*NOTA: Toda la información relativa a las clases y métodos debería estar incluida en los propios ficheros mediante comentarios Javadoc, pero mientras tanto aquí ofrezco una descripción de los principales componentes.*

A continuación, se comentan las principales clases JAVA desarrolladas y la funcionalidad de las mismas: 

- Servicio de tipo "Bound". Este es el componente principal del proyecto (fichero DephisitService.java, anteriormente BluetoothLeService.java).

- Interfaz (API) de comunicación con el servicio (fichero DephisitInterface.java).

- Interfaz que deben implementar los clientes que quieran usar el servicio (fichero DephisitCallback.java).

- Dispositivos DEPHISIT de tipo "detector" desarrollados (ficheros BDDevice.java, TSDDevice.java y WDDevice.java).

- Dispositivos DEPHISIT de tipo "sensor" desarrollados (ficheros LSDevice.java, LSWNDevice.java, PSWNDevice.java, SSWNDevice.java, SeatSWNDevice.java, TSDevice.java, TSWNDevice.java). 

- Dispositivos DEPHISIT de tipo "actuador" desarrollados (ficheros DADevice.java, LADevice.java y SADevice.java).

- Dispositivos DEPHISIT simulados, que pueden ser usados sin necesidad de hardware adicional (ficheros SimulatedPSWNDevice.java, SimulatedSSWNDevice.java y SimulatedTSDDevice.java). Todos estos dispositivos simulados usan los datos incluidos en el fichero SimulationData.java.

- Interfaces implementadas por cada uno de los distintos dispositivos DEPHISIT desarrollados (ficheros XXDeviceI.java).

- Interfaces que deben implementar los clientes que quieran usar aquellos dispositivos DEPHISIT, de cualquier tipo, que tengan llamadas asíncronas (ficheros XXDeviceListener.java).

- Actividades Android de ejemplo que interactúan con el servicio para hacer uso de los dispositivos DEPHISIT: una actividad inicial para escanear los dispositivos DEPHISIT existentes (fichero DeviceScanActivity.java), y una actividad de ejemplo para usar cada uno de los dispositivos DEPHISIT desarrollados.   

- Y el resto de fichero JAVA son clases internas que usa el servicio y/o los dispositivos DEPHISIT.


# Mejoras de la versión 0.1 del servicio Android #

En cuanto a su funcionamiento, en la actualidad este servicio es capaz de descubrir los distintos dispositivos BLE presentes en el entorno y procesarlos, así como generar e interactuar con los distintos dispositivos DEPHISIT definidos hasta el momento:

- Sensor de posición simulados (clase SimulatedPSWNDevice, anteriormente llamada GPSWNDevice en la versión "beta" del servicio). Disponible desde la versión "beta" del servicio.
- Sensor de velocidad simulados (clase SimulatedSSWNDevice, anteriormente llamada SSWNDevice en la versión "beta" del servicio). Disponible desde la versión "beta" del servicio.
- Detector de señales de tráfico simulados (clase SimulatedTrafficSignalDetector, anteriormente llamada TSDDevice en la versión "beta" del servicio). Disponible desde la versión "beta" del servicio.
- Sensor de luz (clases LSDevice y LSWNDevice). Disponible desde la versión "beta" del servicio.
- Sensor de temperatura (clases TSDevice y TSWNDevice). Disponible desde la versión "beta" del servicio.
- Actuador sonoro (clase SADevice). Disponible desde la versión "beta" del servicio.
- Actuador visual (clase DADevice). Disponible desde la versión "beta" del servicio.
- Detector de señales de tráfico (clase TSDDevice). Disponible desde la versión "beta" del servicio (aunque el formato de las advertencias/notificaciones que recibe ha sido modificado en la versión 0.1 del servicio).
- Sensor de posición (clase PSWNDevice). Disponible a partir de la versión 0.1 del servicio.
- Sensor de velocidad (clase SSWNDevice). Disponible a partir de la versión 0.1 del servicio.
- Sensor de seguridad del asiento (clase SeatSWNDevice). Disponible a partir de la versión 0.1 del servicio.
- Actuador luminoso (clase LADevice). Disponible a partir de la versión 0.1 del servicio.
- Detector de las condiciones atmosféricas de la carretera (clase WDDevice). Disponible a partir de la versión 0.1 del servicio.
- Detector de biciletas cercanas (clase BDDevice). Disponible a partir de la versión 0.1 del servicio.

Además, a partir de la versión 0.1, el servicio es capaz de detectar desconexiones por parte de los dispositivos BLE y notificar de ello a la aplicaciones cliente, así como recibir las advertencias emitidas por las distintas balizas presentes en la infraestructura (señales de tráfico, bicicletas y estado de la carretera) de manera constante y sin solapamientos.

Para poder implementar dicho funcionamiento de una forma adecuada, ha sido necesario realizar algunas modificaciones respecto a la versión existente en la última reunión. Entre las modificaciones realizadas cabe destacar las siguientes:

- Los métodos initialize() y scanDephisitDevices() de la interfaz DephisitInterface que existían antes están obsoletos, y ahora en vez de realizar la búsqueda de dispositivos DEPHISIT en dos pasos como antes tan sólo se hace en uno haciendo uso de algunos de los dos nuevos métodos scanDephisitDevices() a lo que se les pasa directamente el “listener”.

```
#!java
public interface DephisitInterface {

	//For use DephisitDevice objects from API client
	@Deprecated
	public boolean initialize(DephisitCallback listener) throws DephisitException;
	@Deprecated
	public boolean scanDephisitDevices(final boolean enable) throws DephisitException;
	public DephisitDevice getDephisitDevice(String deviceId);

	//New methods for scan DPEHIST devices
	public boolean scanDephisitDevices(DephisitCallback listener, boolean enable);
	public boolean scanDephisitDevices(DephisitCallback listener, boolean enable, String regExpId);
	
	. . .
}
```

- El método onScanFinished() de la interfaz DephisitCallback también ha quedado obsoleto.

```
#!java
public interface DephisitCallback {

    //For notificate events to API client
    void onDephisitDevice(String deviceId, boolean available);
    @Deprecated
    void onScanFinished();
}
```

- Algunos de los métodos propios de cada dispositivo DEPHISIT han variado ligeramente respecto a los existentes en la versión anterior del servicio.

- El formato de los mensajes emitidos por los dispositivos de tipo “detector” (las balizas) ha variado un poco desde la última reunión, por lo que la baliza para los semáforos (prototipo hardware basado en la plataforma RFduino) que os dejamos en su momento ya no será reconocida correctamente hasta que no sea actualizada.  


# Ejemplo de uso del servicio desarrollado (tanto para la versión "beta" como para la versión 0.1 y posteriores)#

*NOTA: Todo el código desarrollado hasta ahora se encuentra en su versión 0.1, lo que quiere decir que ya ha pasado su fase "beta". De todas formas, aún sigue existiendo código incompleto y/o que no se esté usando en la versión actual.*

En la versión anterior (versión "beta"), la interfaz para usar el servicio (DephisitInterface) se limita a tres métodos, uno para inicializar el servicio (método initialize(), obsoleto a partir de la versión 0.1 del servicio), otro para iniciar/detener escaneos de los dispositivos DEPHISIT existentes (método scanDephisitDevices(), obsoleto a partir de la versión 0.1 del servicio), y uno tercero (método getDephisitDevice()) para obtener el dispositivo DEPHISIT (objeto DephisitDevice) que permita hacer uso del mismo a través de llamadas directas a sus métodos.
En la versión actual (versión 0.1), el servicio (DephisitService) ha sido mejorado y ahora se permite la interacción con múltiples clientes, para lo cual se han incluido dos nuevos métodos en la interfaz (DephisitInterface) para iniciar/detener escaneos de los dispositivos DEPHISIT existentes (nuevos métodos scanDephisitDevices()) a los que ahora se le pasa el objeto "listener" de las aplicaciones cliente, y opcionalmente un patrón de búsqueda que permite filtar la búsqueda de dispositivos (por ejemplo, limitando la búsqueda de dispositivos a un tipo/localización/nombre concreto).

Por otra parte, la aplicación cliente que quiera usar el servicio deberá definir una clase "listener" que implemente la interfaz DephisitCallback que esta compuesta por los métodos onDephisitDevice() y onScanFinished() (este último obsoleto a partir de la versión 0.1 del servicio), usados para notificar la existencia de un dispositivo DEPHISIT y la finalización del escaneo (previamente lanzado por la aplicación cliente) respectivamente.


A continuación se exponen algunos ejemplos de uso con las llamadas que tendría que hacer la aplicación cliente que quiera hacer una búsqueda de los dispositivos DEPHISIT existentes.

Desde la versión "beta" del servicio, la aplicación cliente puede llamar a los siguientes métodos para realizar la búsqueda de dispositivos DEPHISIT y monitorizar el estado actual (disponible / no disponible) de los mismos:

```
#!java
public class DeviceScanActivityDeprecated extends ListActivity implements DephisitCallback {

	. . .

	//Inicializar el servicio (API)
	@Deprecated
	if (!mBluetoothLeService.initialize(this)) {
		//Error
	}

	//Lanzar un escaneo en busca de dispositivos DEPHISIT existentes
	@Deprecated
	if (!mBluetoothLeService.scanDephisitDevices(true)) {
		//Error
	}

	. . .

}
```

*NOTA: A pesar de que los métodos usados en el código anterior han quedado obsoletos en la actualidad (a partir de la versión 0.1), se pueden seguir usando de la misma manera que antes sin ningún tipo de problema.*

A partir de la versión 0.1 del servicio, ya no hace falta llamar al método initialize(), por lo que la aplicación cliente tan sólo debe llamar al método scanDephisitDevices() al que directamente se le pasa el "listener" (en estos ejemplos, el propio objeto/clase llamante) de la aplicación cliente:

```
#!java
public class DeviceScanActivity extends ListActivity implements DephisitCallback {

	. . .

	//Lanzar un escaneo en busca de dispositivos DEPHISIT existentes
	if (!mBluetoothLeService.scanDephisitDevices(this, true)) {
		//Error
	}

	. . .

}
```

Opcionalmente, la llamada anterior puede ser sustituida por otra que permita afinar más la búsqueda de dispositivos, para lo que se le puede pasar como parámetro adicional un patrón de búsqueda:

```
#!java
public class DeviceScanActivity extends ListActivity implements DephisitCallback {

	. . .

	//Lanzar un escaneo en busca de dispositivos DEPHISIT existentes
	if (!mBluetoothLeService.scanDephisitDevices(this, true, DeviceType.PSWN.name()+":.:.")) { //Buscar sólo dispositivos de tipo PSWN (PositionSensorWithNotifications)
		//Error
	}

	if (!mBluetoothLeService.scanDephisitDevices(this, true, ".:"+Location.DASHBOARD.getCode()+":.")) { //Buscar sólo dispositivos situados en el salpicadero del coche
		//Error
	}

	if (!mBluetoothLeService.scanDephisitDevices(this, true, ".:.:Simulated.")) { //Buscar sólo dispositivos cuyo nombre comience por la cadena de texto "Simulated"
		//Error
	}

	. . .

}
```

Una vez llamado el método scanDephisitDevices() por parte de la aplicación cliente, el objeto "listener" pasado en dicho método empezará a recibir los identificadores de cada uno de los dispositivos DEPHISIT existentes que coincidan con la búsqueda realizada, así como el estado actual de los mismos. Cada vez que se produzca un cambio de estado de los dispositivos en cuestión, se notificará a este objeto "listener" de dicha circunstancia. Para todo ello, el objeto "listener" pasado debe implementar los siguientes métodos definidos por la interfaz DephisitCallback: 

```
#!java
public class DeviceScanActivity extends ListActivity implements DephisitCallback {

	. . .

	public void onDephisitDevice(final String deviceId, final boolean available) {
	
		. . .
	
		if (available) {
			//Mostrar el nuevo dispositivo DEPHISIT encontrado (y con el que existe comunicación). El identificador del dispositivo indica el tipo de dispositivo que es (GPSWN, SSWN, TSD, TS, ...), su localización en el coche en base a un número (puede que esto cambie en un futuro) y su nombre.
		} else {
			//Dejar de mostrar el dispositivo DEPHISIT con el que se ha perdido la comunicación
		}
		
		. . .
	}

	@Deprecated
	public void onScanFinished() {
		//Notificar la finalización del escaneo
	}

	. . . 

}
```


Por último, para usar los dispositivos DEPHISIT devueltos por el servicio tan sólo hay llamar al método getDephisitDevice() pasándole el identificador del mismo, y posteriormente usar los métodos/callbacks propios del dispositivo DEPHISIT. A continuación se muestra un ejemplo de las llamadas y callbacks disponibles para los sensores de posición (dispositivos DEPHISIT que implementan la interfaz PWSNDeviceI):


```
#!java
public class PSWNDeviceActivity extends Activity implements PSWNDeviceListener, DephisitCallback {

	. . .

	//Para recibir notificaciones acerca del estado del dispositivo DEPHISIT cuyo identificador es "PSWN:50:PositionSensor1". 
	//De esta forma se nos notificará, a través del método onDephisitDevice() los posibles cambios de estado (pérdida o reestablecimiento de la comunicación) de dicho dispositivo.
	mBluetoothLeService.scanDephisitDevices(PSWNDeviceActivity.this, true, "PSWN:50:PositionSensor1");
	
	. . .
	
	//Para empezar a interactuar con el dispositivo "PSWN:50:PositionSensor1" a través de los métodos proporcionados por la interfaz que implementa (PSWNDeviceI).
	//Los métodos definidos por la interfaz PSWNDeviceI son: getPosition(), enableNotifications(), areNotificationsEnabled() y disableNotifications().
	PSWNDeviceI device = (PSWNDeviceI)mBluetoothLeService.getDephisitDevice("PSWN:50:PositionSensor1");

	. . . 
	
	//Obtener la posición actual. El resultado será devuelto a través del método onPositionValue().
	device.getPosition(PSWNDeviceActivity.this);
	
	. . .
	
	//Habilitar las notificaciones para este dispositivo. Cada vez que la posición cambie seremos notificados a través del método onPositionChange().
	device.enableNotifications(PSWNDeviceActivity.this);	
	
	. . .
	
	//Obtener el un umbral actualmente usado para notificar los cambios de posición. 
	float threshold = device.getThreshold();
	
	. . .
	
	//Establecer un umbral de 0.001 para los cambios de posición. 
	//Sólo seremos notificados si la posición (latitud y/o longitud) varía en más de 0.001 con respecto a la posición anterior.
	device.setThreshold(0.001f);
	
	. . .
	
	//Consultar el estado actual de las notificaciones para este dispositivo.
	boolean enabled = device.areNotificationsEnabled();	
	
	. . .
	
	//Deshabilitar las notificaciones para este dispositivo. Dejaremos de ser notificados en relación con los cambios de posición.
	device.disableNotifications(PSWNDeviceActivity.this);
	
	. . .

	//Notificaciones relativas al estado de disponibilidad del dispositivo que estamos usando
	//(y del que queremos ser notificados en el caso de que cambie su estado durante su uso).
	public void onDephisitDevice(final String deviceId, final boolean available) {
		. . .
	
		if (available) {
			//El dispositivo con identificador "PSWN:50:PositionSensor1" está disponible para ser usado (el servicio dispone de comunicación con dicho dispositivo).
		} else {
			//El dispositivo con identificador "PSWN:50:PositionSensor1" ha dejado de estar disponible para ser usado (se ha perdido la comunicación con dicho dispositivo).
		}
		
		. . .
	}
	
	. . .
	
	//Notificaciones con el valor de la posición actual que ha sido solicitada mediante la correspondiente llamada al método getPosition(). 
	public void onPositionValue(PSWNDeviceI pswnDevice, final float latitude, final float longitude) {
		//Mostrar la posición actual 
	}

	. . .
	
	//Notificaciones con el valor de la nueva posición. 
	//Estas notificaciones sólo las recibiremos después de llamar al método enableNotifications(), y dejaremos de recibirlas tras llamar al método disableNotifications().
	public void onPositionChange(PSWNDeviceI pswnDevice, final float latitude, final float longitude) {
		//Mostrar la nueva posición
	}

	. . .

}
```

*NOTA: Para hacer uso de otros tipos de dispositivos DEPHISIT (distintos a PSWN), tan sólo hay que definir el dispositivo (objeto "device" del código anterior) en base a la correspondiente interfaz XXDeviceI y hacer uso de los distintos métodos definidos por la misma. 
De igual modo, para recibir las correspondientes notificaciones relativas a dicho tipo de dispositivo, nuestra clase (aplicación) cliente debe implementar la correspondiente interfaz XXDeviceListener.*