/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

//TODO Synchronize calls?
//TODO Create a common interface for this type of devices (detectors)

/**
 * SimulatedTSDDevice - Implementation of a simulated TrafficSignalsDetector device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class SimulatedTSDDevice extends SimulatedDephisitDevice implements TSDDeviceI {
	//private final static String TAG = SimulatedTSDDevice.class.getSimpleName();
	
	private TSDDeviceListener trafficSignalsDetectorDListener = null;
	
	Thread simulatedTrafficSignalsThread = new Thread(new Runnable() {
        public void run() {
        	float[] currentPosition = SimulationData.SIMULATED_COORDINATES[0];
        	
        	while (true) {
        		for (int i=0; i<SimulationData.SIMULATED_COORDINATES.length; i++) {

        			try {
            			//Wait TIME_INTERVAL miliseconds between each step
						Thread.sleep(SimulationData.TIME_INTERVAL);
						
						currentPosition = SimulationData.SIMULATED_COORDINATES[i];
	 
						for (int[] detection : SimulationData.TRAFFIC_SIGNALS_DETECTIONS) {
							//Notify old traffic signals out of range
							if (detection[0]==i && simulatedNotifications) {
								trafficSignalsDetectorDListener.onTrafficSignOutOfRange(TrafficSign.fromCode(String.valueOf(detection[1])), currentPosition[1], currentPosition[0], detection[2], detection[3]);
							}
							
							//Notify new traffic signals in range
							if (detection[0]==i+1 && simulatedNotifications) {
								trafficSignalsDetectorDListener.onNewTrafficSignInRange(TrafficSign.fromCode(String.valueOf(detection[1])), currentPosition[1], currentPosition[0], detection[2], detection[3]);
							}
						}
	            		
        			} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        		}
            }
        }
    });
	
	private boolean simulatedNotifications = false;
	
	public SimulatedTSDDevice(String n, Location l) {
		super(DeviceType.TSD, n, l);
		
		//Simulate this device 
		simulatedTrafficSignalsThread.start();
	}
	
	//TODO Change for use multiple clients???
	@Override
	public boolean areNotificationsEnabled() {
		//return this.trafficSignalsDetectorDListener!=null;
		return this.simulatedNotifications;
	}
	
	@Override
	public boolean enableNotifications(TSDDeviceListener trafficSignalsDetectorDListener) {
		this.trafficSignalsDetectorDListener = trafficSignalsDetectorDListener;
		this.simulatedNotifications = true;
		
		return true;
	}
	
	@Override
	public boolean disableNotifications(TSDDeviceListener trafficSignalsDetectorDListener) {
		this.trafficSignalsDetectorDListener = null;
		this.simulatedNotifications = false;
		
		return true;
	}
}
