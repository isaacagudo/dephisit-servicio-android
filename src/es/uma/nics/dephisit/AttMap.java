/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import java.util.UUID;

/**
 * AttMap - Implementation of the class used for define attributes mappings.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class AttMap {
	UUID serviceUUID;
	UUID characteristicUUID;
	
	AttMap (String sUUID, String cUUID) {
		serviceUUID = UUID.fromString(sUUID);
		characteristicUUID = UUID.fromString(cUUID);
	}
}
