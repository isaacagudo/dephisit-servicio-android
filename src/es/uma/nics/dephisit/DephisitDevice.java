/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;

/**
 * DephisitDevice - Abstract class that define common functionality of all DEPHISIT actuator and sensor devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public abstract class DephisitDevice implements DephisitDeviceI {

	private final String name;
	private final Location location;
	private final DeviceType type;
	
	//TODO Necessary? -> Not used currently
	private boolean inRange;
	
	protected BluetoothGatt bleDevice;
	
	protected DephisitService bluetoothLeService;
	
	//TODO Define callback in this class?
	//protected DephisitDeviceCallback callback;
	
	
	public DephisitDevice(DeviceType type, String name, Location location, BluetoothGatt bleDevice, DephisitService bluetoothLeService) {
		this.type = type;
		this.name = name;
		this.location = location;
		this.bleDevice = bleDevice;
		this.bluetoothLeService = bluetoothLeService;
		this.inRange = true;
	}

	public String getName() {
		return name;
	}

	public Location getLocation() {
		return location;
	}
	
	public DeviceType getType() {
		return type;
	}
	
	public String getDeviceId() {
		return type.name() + ":" + location.getCode() + ":" + name; 
	}
	
	abstract boolean isComplete();
	
	abstract void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic);
	
	BluetoothGatt getBluetoothGatt() {
		return bleDevice;
	}
	
	protected void setInRange(boolean inRange) {
		this.inRange = inRange;
		
		/*if (!this.inRange) {
			//TODO Send notification to client/s
		}*/
	}
	
	protected boolean getInRange() {
		return inRange;
	}
}
