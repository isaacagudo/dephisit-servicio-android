/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * TSWNDeviceListener - Interface to be implemented by clients that use TemperatureSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface TSWNDeviceListener {

	void onTempValue(TSWNDeviceI tswnDevice, float value);
	void onTempChange(TSWNDeviceI tswnDevice, float value);
	
}
