/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

/**
 * PSWNDevice - Implementation of the PositionSensorWithNotifications device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class PSWNDevice extends DephisitDevice implements PSWNDeviceI {
	private final static String TAG = PSWNDevice.class.getSimpleName();
	
	private BluetoothGattCharacteristic positionValue = null;
	private BluetoothGattCharacteristic positionThreshold = null;
	
	private PSWNDeviceListener pswnDListener = null;
	
	//TODO Use synchronization?
	//private Boolean free = true;
	//private final Object lock = new Object();
	
	private static enum PSWNChar {PSWN_POSITION_VALUE, PSWN_POSITION_THRESHOLD}
	
	//Read and Notification attribute
	static private final AttMap[] PSWN_POSITION_VALUE_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000001-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002221-0000-1000-8000-00805f9b34fb")
	};
	
	//Write attribute
	static private final AttMap[] PSWN_POSITION_THRESHOLD_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000002-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002222-0000-1000-8000-00805f9b34fb")
	};
	
	private final DephisitDeviceCallback callback = new DephisitDeviceCallback() {

		@Override
		public void onValue(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValue() - characteristic: " + characteristic.getUuid());
			
			if (characteristic==positionValue) {
				
				//synchronized(lock) {
				
				pswnDListener.onPositionValue(PSWNDevice.this, DephisitService.getFloatLEValueFromCharacteristic(characteristic, 0), DephisitService.getFloatLEValueFromCharacteristic(characteristic, 4));				
				
				//free = true;
				//free.notify();
				//}
			}
		}
		
		@Override
		public void onValueChange(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValueChange() - characteristic: " + characteristic.getUuid());
			
			if (characteristic==positionValue) {
				
				pswnDListener.onPositionChange(PSWNDevice.this, DephisitService.getFloatLEValueFromCharacteristic(characteristic, 0), DephisitService.getFloatLEValueFromCharacteristic(characteristic, 4));
			}
		}
	};
	
	public PSWNDevice(String n, Location l, BluetoothGatt bleDevice, DephisitService bluetoothLeService) {
		super(DeviceType.PSWN, n, l, bleDevice, bluetoothLeService);
	}

	@Override
	public boolean getPosition(PSWNDeviceListener pswnDListener) {
		Log.d(TAG, "getTemp() - pswnDListener: " + pswnDListener);
		
		//
		/*synchronized(lock) {
			if (!free) {
				try {
					free.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			this.pswnDListener = pswnDListener;
			free = false;
			
			return bluetoothLeService.readCharacteristic(bleDevice, positionValue, callback);
		}*/
		this.pswnDListener = pswnDListener;
		
		return DephisitService.readCharacteristic(bleDevice, positionValue, callback);
	}
	
	//TODO Change for use multiple clients???
	@Override
	public boolean areNotificationsEnabled() {
		return DephisitService.areNotificationsEnabled(positionValue);
	}
	
	@Override
	public boolean enableNotifications(PSWNDeviceListener pswnDListener) {
		this.pswnDListener = pswnDListener;
		return DephisitService.setCharacteristicNotification(bleDevice, positionValue, true, callback);
	}
	
	@Override
	public boolean disableNotifications(PSWNDeviceListener pswnDListener) {
		this.pswnDListener = null;
		return DephisitService.setCharacteristicNotification(bleDevice, positionValue, false, null);
	}
	
	@Override
	public float getThreshold() {
		Log.d(TAG, "getThreshold()");
		
		return DephisitService.getFloatLEValueFromCharacteristic(positionThreshold);
	}
	
	@Override
	public boolean setThreshold(float threshold) {
		Log.d(TAG, "setThreshold() - threshold: " + threshold);
		
		if (threshold > 0.0 && threshold <= 1.0) {
			DephisitService.setFloatLEValueToCharacteristic(positionThreshold, threshold);
			return DephisitService.writeCharacteristic(bleDevice, positionThreshold);
		} else {
			Log.w(TAG, "Ignoring threshold value (value: " + threshold + ")");
			return false;
		}
	}

	static String isValidCharacteristic(BluetoothGattCharacteristic characteristic) {
		for (AttMap map : PSWN_POSITION_VALUE_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) > 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
				return PSWNChar.PSWN_POSITION_VALUE.toString();
			}
		}
		
		for (AttMap map : PSWN_POSITION_THRESHOLD_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {
				return PSWNChar.PSWN_POSITION_THRESHOLD.toString();
			}
		}
		
		return null;
	}
	
	@Override
	public boolean isComplete() {
		return positionValue!=null && positionThreshold!=null;
	}

	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		switch (PSWNChar.valueOf(charName)) {
		case PSWN_POSITION_VALUE:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'PositionValue' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.positionValue = characteristic;
			break;
		case PSWN_POSITION_THRESHOLD:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'PositionThreshold' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.positionThreshold = characteristic;
			break;
		default:
			break;
		}
	}
}
