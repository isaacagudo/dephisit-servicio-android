/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * LSWNDeviceListener - Interface to be implemented by clients that use LightSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface LSWNDeviceListener {

	void onLightValue(LSWNDeviceI lswnDevice, int value);
	void onLightChange(LSWNDeviceI lswnDevice, int value);
	
}
