/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * DephisitBeacon - DEPHISIT's beacons identifiers and types.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class DephisitBeacon {
	
	public final static byte mdEIRType = (byte)0xFF;
	public final static byte[] dephisitBeaconId = {(byte)0xDE, (byte)0xBE};
	
	public enum DephisitBeaconType {
		UNKNOWN(0x00, "Unknown"),
		
		TSBEACON(0x01, "Traffic Sign Beacon"),
	    WBEACON(0x02, "Weather Beacon"),
	    BBEACON(0x03, "Bicycle Beacon");

		private final int code;
	    private final String name;
		
		DephisitBeaconType(int code, String name) {
	    	this.code = code;
	    	this.name = name;
	    }
		
		public int getCode() {
			return code;
		}
		
		public String toString(){
			return name;
		}
		
		static public DephisitBeaconType fromCode(String code) {
			DephisitBeaconType dbType = DephisitBeaconType.UNKNOWN;
			for (DephisitBeaconType dbt : DephisitBeaconType.values()) {
				if (dbt.getCode() == Integer.valueOf(code).intValue()) {
					dbType = dbt;
				}
			}
			return dbType;
		}
	}
}
