/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * DephisitInterface - Interface to be implemented by clients that use DEPHISIT's service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface DephisitCallback {

    //For notificate events to API client
    void onDephisitDevice(String deviceId, boolean available);
    @Deprecated
    void onScanFinished();
}

