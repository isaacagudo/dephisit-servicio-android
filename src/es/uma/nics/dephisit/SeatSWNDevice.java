/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

/**
 * SeatSWNDevice - Implementation of the SeatSensorWithNotifications device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class SeatSWNDevice extends DephisitDevice implements SeatSWNDeviceI {
	private final static String TAG = SeatSWNDevice.class.getSimpleName();
	
	private BluetoothGattCharacteristic seatValue = null;
	
	private SeatSWNDeviceListener seatswnDListener = null;
	
	//TODO Use synchronization?
	//private Boolean free = true;
	//private final Object lock = new Object();
	
	private static enum SeatSWNChar {SeatSWN_SEAT_VALUE}

	//Read and Notification attribute
	static private final AttMap[] SeatSWN_SEAT_VALUE_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000005-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002221-0000-1000-8000-00805f9b34fb")
	};
	
	private final DephisitDeviceCallback callback = new DephisitDeviceCallback() {

		@Override
		public void onValue(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValue() - characteristic: " + characteristic.getUuid());
			
			if (characteristic==seatValue) {
				
				//synchronized(lock) {
				
				seatswnDListener.onSeatStateValue(SeatSWNDevice.this, DephisitService.getByteValueFromCharacteristic(characteristic) == 0x00);
				
				//free = true;
				//free.notify();
				//}
			}
		}
		
		@Override
		public void onValueChange(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValueChange() - characteristic: " + characteristic.getUuid());
			
			if (characteristic==seatValue) {
				
				seatswnDListener.onSeatStateChange(SeatSWNDevice.this, DephisitService.getByteValueFromCharacteristic(characteristic) == 0x00);
			}
		}
	};
	
	public SeatSWNDevice(String n, Location l, BluetoothGatt bleDevice, DephisitService bluetoothLeService) {
		super(DeviceType.SeatSWN, n, l, bleDevice, bluetoothLeService);
	}

	@Override
	public boolean getSeatState(SeatSWNDeviceListener seatswnDListener) {
		Log.d(TAG, "getSeatState() - seatswnDListener: " + seatswnDListener);
		
		//
		/*synchronized(lock) {
			if (!free) {
				try {
					free.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			this.lswnDListener = lswnDListener;
			free = false;
			
			return bluetoothLeService.readCharacteristic(bleDevice, lightValue, callback);
		}*/
		this.seatswnDListener = seatswnDListener;
		
		return DephisitService.readCharacteristic(bleDevice, seatValue, callback);
	}
	
	//TODO Change for use multiple clients???
	@Override
	public boolean areNotificationsEnabled() {
		return DephisitService.areNotificationsEnabled(seatValue);
	}
	
	@Override
	public boolean enableNotifications(SeatSWNDeviceListener seatswnDListener) {
		this.seatswnDListener = seatswnDListener;
		return DephisitService.setCharacteristicNotification(bleDevice, seatValue, true, callback);
	}
	
	@Override
	public boolean disableNotifications(SeatSWNDeviceListener seatswnDListener) {
		this.seatswnDListener = null;
		return DephisitService.setCharacteristicNotification(bleDevice, seatValue, false, null);
	}
	
	static String isValidCharacteristic(BluetoothGattCharacteristic characteristic) {	
		for (AttMap map : SeatSWN_SEAT_VALUE_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) > 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
				return SeatSWNChar.SeatSWN_SEAT_VALUE.toString();
			}
		}
		
		return null;
	}
	
	@Override
	public boolean isComplete() {
		return seatValue!=null;
	}

	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		switch (SeatSWNChar.valueOf(charName)) {
		case SeatSWN_SEAT_VALUE:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'SeatValue' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.seatValue = characteristic;
			break;
		default:
			break;
		}
	}	
}
