/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * DephisitException - Exception to be used in DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
@Deprecated
public class DephisitException extends Exception {

	private static final long serialVersionUID = 1L;

	DephisitException(String message) {
		super(message);
	}
}
