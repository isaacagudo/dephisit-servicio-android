/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * SSWNDeviceListener - Interface to be implemented by clients that use SpeedSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface SSWNDeviceListener {

	void onSpeedValue(SSWNDeviceI speedswnDevice, int speed);
	void onSpeedChange(SSWNDeviceI speedswnDevice, int speed);
}
