/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * WDDeviceListener - Interface to be implemented by clients that use WeatherDetector devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface WDDeviceListener {

	void onNewWeatherWarningInRange(RoadState type, float latitude, float longitude);
	void onWeatherWarningOutOfRange(RoadState type, float latitude, float longitude);
}
