/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * BDDeviceListener - Interface to be implemented by clients that use BicycleDetector devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface BDDeviceListener {
	
	void onNewBicycleWarningInRange(BicycleState type, float latitude, float longitude);
	void onBicycleWarningOutOfRange(BicycleState type, float latitude, float longitude);
}
