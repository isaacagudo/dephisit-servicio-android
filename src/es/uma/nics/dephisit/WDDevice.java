/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;


//TODO Synchronize calls?
//TODO Create a common interface for this type of devices (detectors)

/**
 * WDDevice - Implementation of the WeatherDetector device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class WDDevice extends DephisitDevice implements WDDeviceI {
	private final static String TAG = WDDevice.class.getSimpleName();
	
	private WDDeviceListener weatherDetectorDListener = null;
	
	private volatile Map<String, WBeacon> wBeaconsTime = new HashMap<String, WBeacon>();
	
	//private final Object lock = new Object();
	
	class WBeacon {
		protected float latitude;
		protected float longitude;
		protected byte roadState;
		protected long lastTime;
		
		protected WBeacon(float latitude, float longitude, byte roadState, long lastTime) {
			this.latitude = latitude;
			this.longitude = longitude;
			this.roadState = roadState;
			this.lastTime = lastTime;
		}
	}
	
	Thread wdThread = new Thread(new Runnable() {
        public void run() {
        	
        	List<String> keysToDelete = new ArrayList<String>();
        	
        	while (true) {
        		//synchronized (lock) {
        		for (Entry<String, WBeacon> entryBeacon: wBeaconsTime.entrySet()) {
        			WBeacon wBeacon = entryBeacon.getValue();
        			String wBeaconId = entryBeacon.getKey();
        			if (System.currentTimeMillis() - 2000 > wBeacon.lastTime) {
        				if (weatherDetectorDListener != null) {
        					weatherDetectorDListener.onWeatherWarningOutOfRange(RoadState.fromCode(String.valueOf(wBeacon.roadState)),
        							wBeacon.latitude, wBeacon.longitude);
        				}
        				
        				//Delete traffic sign beacon
        				keysToDelete.add(wBeaconId);
        			}
        		}
        		
        		for (String key: keysToDelete) {
        			wBeaconsTime.remove(key);
        		}
        		keysToDelete.clear();
        		//}
        		
        		try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
            }
        }
    });
	

	public WDDevice(String n, Location l) {
		super(DeviceType.WD, n, l, null, null);
		
		wdThread.start();
	}
	
	//TODO Change for use multiple clients???
	@Override
	public boolean areNotificationsEnabled() {
		return this.weatherDetectorDListener!=null;
	}
	
	@Override
	public boolean enableNotifications(WDDeviceListener weatherDetectorDListener) {
		this.weatherDetectorDListener = weatherDetectorDListener;
		
		//synchronized (lock) {
		for (WBeacon wBeacon: wBeaconsTime.values()) {
			weatherDetectorDListener.onNewWeatherWarningInRange(RoadState.fromCode(String.valueOf(wBeacon.roadState)),
					wBeacon.latitude, wBeacon.longitude);
		//}
		}
		
		return true;
	}
	
	@Override
	public boolean disableNotifications(WDDeviceListener weatherDetectorDListener) {
		this.weatherDetectorDListener = null;
		
		return true;
	}
	
	@Override
	public boolean isComplete() {
		return true;
	}

	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		//Do nothing, this is a detector device
	}
	
	void notifyAdvertisement(String id, float latitude, float longitude, byte roadState) {
		Log.d(TAG, "Weather Advertisement - id: " + id + ", latitude: " + latitude + ", longitude: " + longitude 
				+ "roadState: " + roadState);
		
		WBeacon wBeacon = new WBeacon(latitude, longitude, roadState, System.currentTimeMillis());
	
		if (RoadState.fromCode(String.valueOf(roadState)) != RoadState.DRY) {
		
		//synchronized (lock) {
		if (wBeaconsTime.containsKey(id) && wBeaconsTime.get(id).roadState == roadState) {
			wBeaconsTime.put(id, wBeacon);
			
		} else {
			//Put state in the map
			wBeaconsTime.put(id, wBeacon);

			//Notify state to listener
			if (weatherDetectorDListener != null) {
				weatherDetectorDListener.onNewWeatherWarningInRange(RoadState.fromCode(String.valueOf(roadState)),
						wBeacon.latitude, wBeacon.longitude);
			}
		}
		//}
		
		}
	}
}
