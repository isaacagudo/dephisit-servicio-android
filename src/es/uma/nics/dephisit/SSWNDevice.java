/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

/**
 * SSWNDevice - Implementation of the SpeedSensorWithNotifications device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class SSWNDevice extends DephisitDevice implements SSWNDeviceI {
	private final static String TAG = SSWNDevice.class.getSimpleName();
	
	private BluetoothGattCharacteristic speedValue = null;
	private BluetoothGattCharacteristic speedThreshold = null;
	
	private SSWNDeviceListener sswnDListener = null;
	
	//TODO Use synchronization?
	//private Boolean free = true;
	//private final Object lock = new Object();
	
	private static enum SSWNChar {SSWN_SPEED_VALUE, SSWN_SPEED_THRESHOLD}
	
	//Read and Notification attribute
	static private final AttMap[] SSWN_SPEED_VALUE_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000003-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002221-0000-1000-8000-00805f9b34fb")
	};
	
	//Write attribute
	static private final AttMap[] SSWN_SPEED_THRESHOLD_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000004-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002222-0000-1000-8000-00805f9b34fb")
	};
	
	
	private final DephisitDeviceCallback callback = new DephisitDeviceCallback() {

		@Override
		public void onValue(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValue() - characteristic: " + characteristic.getUuid());
			
			if (characteristic==speedValue) {
				
				//synchronized(lock) {
				
				sswnDListener.onSpeedValue(SSWNDevice.this, DephisitService.getIntLEValueFromCharacteristic(characteristic));
				
				//free = true;
				//free.notify();
				//}
			}
		}
		
		@Override
		public void onValueChange(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValueChange() - characteristic: " + characteristic.getUuid());
			
			if (characteristic==speedValue) {
				
				sswnDListener.onSpeedChange(SSWNDevice.this, DephisitService.getIntLEValueFromCharacteristic(characteristic));
			}
		}
	};
	
	public SSWNDevice(String n, Location l, BluetoothGatt bleDevice, DephisitService bluetoothLeService) {
		super(DeviceType.SSWN, n, l, bleDevice, bluetoothLeService);
	}

	@Override
	public boolean getSpeed(SSWNDeviceListener sswnDListener) {
		Log.d(TAG, "getSpeed() - sswnDListener: " + sswnDListener);
		
		/*synchronized(lock) {
			if (!free) {
				try {
					free.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			this.lswnDListener = lswnDListener;
			free = false;
			
			return bluetoothLeService.readCharacteristic(bleDevice, lightValue, callback);
		}*/
		this.sswnDListener = sswnDListener;
		
		return DephisitService.readCharacteristic(bleDevice, speedValue, callback);
	}
	
	//TODO Change for use multiple clients???
	@Override
	public boolean areNotificationsEnabled() {
		return DephisitService.areNotificationsEnabled(speedValue);
	}
	
	@Override
	public boolean enableNotifications(SSWNDeviceListener sswnDListener) {
		this.sswnDListener = sswnDListener;
		return DephisitService.setCharacteristicNotification(bleDevice, speedValue, true, callback);
	}
	
	@Override
	public boolean disableNotifications(SSWNDeviceListener sswnDListener) {
		this.sswnDListener = null;
		return DephisitService.setCharacteristicNotification(bleDevice, speedValue, false, null);
	}
	
	@Override
	public int getThreshold() {
		Log.d(TAG, "getThreshold()");
		
		return DephisitService.getIntLEValueFromCharacteristic(speedThreshold);
	}
	
	@Override
	public boolean setThreshold(int threshold) {
		Log.d(TAG, "setThreshold() - threshold: " + threshold);
		
		if (threshold > 0 && threshold <= 10) {
			DephisitService.setIntLEValueToCharacteristic(speedThreshold, threshold);
			return DephisitService.writeCharacteristic(bleDevice, speedThreshold);
		} else {
			Log.w(TAG, "Ignoring threshold value (value: " + threshold + ")");
			return false;
		}
		
	}

	static String isValidCharacteristic(BluetoothGattCharacteristic characteristic) {	
		for (AttMap map : SSWN_SPEED_VALUE_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) > 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
				return SSWNChar.SSWN_SPEED_VALUE.toString();
			}
		}
		
		for (AttMap map : SSWN_SPEED_THRESHOLD_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {
				return SSWNChar.SSWN_SPEED_THRESHOLD.toString();
			}
		}
		
		return null;
	}
	
	@Override
	public boolean isComplete() {
		return speedValue!=null && speedThreshold!=null;
	}

	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		switch (SSWNChar.valueOf(charName)) {
		case SSWN_SPEED_VALUE:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'SpeedValue' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.speedValue = characteristic;
			break;
		case SSWN_SPEED_THRESHOLD:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'SpeedThreshold' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.speedThreshold = characteristic;
			break;
		default:
			break;
		}
	}
}
