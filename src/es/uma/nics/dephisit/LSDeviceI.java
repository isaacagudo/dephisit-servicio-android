/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * LSDeviceI - Interface to be implemented by LightSensor devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface LSDeviceI extends DephisitDeviceI {
	
	boolean getLight(LSDeviceListener lsDListener);
}
