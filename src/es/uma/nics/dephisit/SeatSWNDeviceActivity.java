/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

/**
 * SeatSWNDeviceActivity - Activity for use SeatSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class SeatSWNDeviceActivity extends Activity implements SeatSWNDeviceListener, DephisitCallback {

	private final static String TAG = SeatSWNDeviceActivity.class.getSimpleName();
	
	private DephisitService mBluetoothLeService;
	
	private String deviceId;
	private SeatSWNDeviceI device;
	
	private boolean mBound;
	
	// Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
        	mBluetoothLeService = ((DephisitService.LocalBinder) service).getService();
        	mBound = true;
        	
        	Log.i(TAG, "BluetoothLeService (API) connected");
        	
        	/*device = (SeatSWNDeviceI) mBluetoothLeService.getDephisitDevice(deviceId); 
        	
        	runOnUiThread(new Runnable() {
    			public void run() {
    				((Button)findViewById(R.id.seatswnGetSeatState)).setEnabled(true);
    				
    				//((Switch)findViewById(R.id.switch1)).setChecked(device.areNotificationsEnabled());
    				((Switch)findViewById(R.id.seatswnNotifications)).setEnabled(true);
    			}
    		});*/
        	
        	mBluetoothLeService.scanDephisitDevices(SeatSWNDeviceActivity.this, true, deviceId);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        	mBluetoothLeService = null;
        	mBound = false;
        	
        	Log.i(TAG, "BluetoothLeService (API) disconnected");
        }
    };
	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_seatswndevice);
		
		Log.d(TAG, "onCreate()");
		
		Intent intent = getIntent();
		
		deviceId = intent.getStringExtra("deviceId");

		Log.d(TAG, "onCreate() - deviceId:" + deviceId);
		
		((TextView)findViewById(R.id.seatswnDeviceId)).setText(deviceId);
		
		Button button = (Button)findViewById(R.id.seatswnGetSeatState);
		button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	device.getSeatState(SeatSWNDeviceActivity.this);
            }
        });
		button.setEnabled(false);
		
		final Switch sw = (Switch)findViewById(R.id.seatswnNotifications);
		sw.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		   @Override
		   public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		 
			   if (sw.isChecked()) {
		    		Toast.makeText(getApplicationContext(), "Notifications enabled", Toast.LENGTH_SHORT).show();
		    		device.enableNotifications(SeatSWNDeviceActivity.this);
		    	} else {
		    		Toast.makeText(getApplicationContext(), "Notifications disabled", Toast.LENGTH_SHORT).show();
		    		device.disableNotifications(SeatSWNDeviceActivity.this);
		    	}
		 
		   }
		});
		sw.setEnabled(false);
	}
	
	@Override
    protected void onStart() {
    	Log.d(TAG, "onStart()");
    	
        super.onStart();

        // Bind to LocalService
        Intent gattServiceIntent = new Intent(this, DephisitService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }
    
    @Override
    protected void onStop() {
    	Log.d(TAG, "onStop()");
    	
        super.onStop();
        
        // Unbind from the service
        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }
    }

    public void onNotificationsClicked(View v)
    {
    	Switch sw = (Switch)v;
    	
    	if (sw.isChecked()) {
    		Toast.makeText(getApplicationContext(), "Notifications enabled", Toast.LENGTH_SHORT).show();
    		device.enableNotifications(this);
    	} else {
    		Toast.makeText(getApplicationContext(), "Notifications disabled", Toast.LENGTH_SHORT).show();
    		device.disableNotifications(this);
    	}
    }
    
	@Override
	public void onSeatStateValue(SeatSWNDeviceI seatswnDevice, final boolean value) {
		runOnUiThread(new Runnable() {
			public void run() {
				((TextView)findViewById(R.id.seatswnSeatValue)).setText(value?"SAFE":"NON-SAFE");
			}
		});
	}

	@Override
	public void onSeatStateChange(SeatSWNDeviceI seatswnDevice, final boolean value) {
		runOnUiThread(new Runnable() {
			public void run() {
				((TextView)findViewById(R.id.seatswnSeatValue)).setText(value?"SAFE":"NON-SAFE");
			}
		});
	}

	@Override
	public void onDephisitDevice(final String deviceId, final boolean available) {
		//if (device.getDeviceId().equals(deviceId)) {
			runOnUiThread(new Runnable() {
				public void run() {
					if (available) {
						device = (SeatSWNDeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
						
						((Button)findViewById(R.id.seatswnGetSeatState)).setEnabled(true);
						((Switch)findViewById(R.id.seatswnNotifications)).setEnabled(true);
						Toast.makeText(getApplicationContext(), "DEPHISIT device available ", Toast.LENGTH_SHORT).show();
					} else {
						//device = null;
						
						((Button)findViewById(R.id.seatswnGetSeatState)).setEnabled(false);
						((Switch)findViewById(R.id.seatswnNotifications)).setEnabled(false);
						Toast.makeText(getApplicationContext(), "DEPHISIT device unavailable", Toast.LENGTH_SHORT).show();
						
					}
				}
			});
		//}
	}

	@Override
	@Deprecated
	public void onScanFinished() {
		// TODO Auto-generated method stub
		
	}
}
