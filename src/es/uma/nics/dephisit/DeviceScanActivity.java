/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.app.Activity;
import android.app.ListActivity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * DephisitScanActivity - Main activity to scan and to list DEPHISIT devices found.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class DeviceScanActivity extends ListActivity implements DephisitCallback {

	private final static String TAG = DeviceScanActivity.class.getSimpleName();
	
	private LeDeviceListAdapter mLeDeviceListAdapter;
    private DephisitService mBluetoothLeService;
    
    private static final int REQUEST_ENABLE_BT = 1;
    
    private boolean mBound = false;
    private boolean mScanning = false;
    
    /*MESSENGER
    private Messenger messenger;
    
    class ResponseHandler extends Handler {
    	 
        @Override
        public void handleMessage(Message msg) {
            int respCode = msg.what;
     
            switch (respCode) {
	            case BluetoothLeService.DEPHISIT_ON_NEW_DEVICE: {
	                final String deviceId = msg.getData().getString("deviceId");
	                final boolean inRange = msg.getData().getBoolean("inRange");
	                
	                runOnUiThread(new Runnable() {
	                    @Override
	                    public void run() {
	                    	if (inRange) {
	                    		mLeDeviceListAdapter.addDevice(deviceId);
	                    	} else {
	                    		mLeDeviceListAdapter.removeDevice(deviceId);
	                    	}
	                        mLeDeviceListAdapter.notifyDataSetChanged();
	                    }
	                });
	            }
            }
        }
     
    }*/
    
    
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
        	mBluetoothLeService = ((DephisitService.LocalBinder) service).getService();
        	mBound = true;
        	
        	Log.i(TAG, "BluetoothLeService (API) connected");
        	
        	/*DEPRECATED
        	try {
        		//Initialize service (API), Bluetooth must be enabled
				if (!mBluetoothLeService.initialize(DeviceScanActivity.this)) {
					Toast.makeText(getApplicationContext(), "Error calling API.initialize() method", Toast.LENGTH_SHORT).show();
				}
				
				//Scan Dephisit devices
	        	if (!mBluetoothLeService.scanDephisitDevices(true)) {
	        		Toast.makeText(getApplicationContext(), "Error calling API.scanDephisitDevices() method", Toast.LENGTH_SHORT).show();
	        	} else {
	        		mScanning = true;
	        		invalidateOptionsMenu();
	                Toast.makeText(getApplicationContext(), "Scanning Dephisit devices...", Toast.LENGTH_SHORT).show();
	        	}
			} catch (DephisitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/

        	if (!mBluetoothLeService.scanDephisitDevices(DeviceScanActivity.this, true)) {
        		Toast.makeText(getApplicationContext(), "Error calling API.scanDephisitDevices() method", Toast.LENGTH_SHORT).show();
        	} else {
        		mScanning = true;
        		invalidateOptionsMenu();
                Toast.makeText(getApplicationContext(), "Scanning Dephisit devices...", Toast.LENGTH_SHORT).show();
        	}
        	
        	/*MESSENGER
        	messenger = new Messenger(service);
        	mBound = true;
        	
        	Message msg = Message.obtain(null, BluetoothLeService.DEPHISIT_SCAN_DEVICES);
	        msg.replyTo = new Messenger(new ResponseHandler());
			try {
				messenger.send(msg);
			} catch (RemoteException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}*/
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        	mBluetoothLeService = null;
        	mBound = false;
        	
        	Log.i(TAG, "BluetoothLeService (API) disconnected");
        }
    };
    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (!mScanning) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress);
        }
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        
            case R.id.menu_scan:
            	/*mLeDeviceListAdapter.clear();
                mLeDeviceListAdapter.notifyDataSetChanged();*/
 
				/*DEPRECATED
				try {
					if (!mBluetoothLeService.scanDephisitDevices(true)) {
	                	Toast.makeText(getApplicationContext(), "Error calling API.scanDephisitDevices() method", Toast.LENGTH_SHORT).show();
	            	} else {
	            		mScanning = true;
	            		invalidateOptionsMenu();
	                    Toast.makeText(getApplicationContext(), "Scanning Dephisit devices...", Toast.LENGTH_SHORT).show();
	            	}
				} catch (DephisitException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				
            	if (!mBluetoothLeService.scanDephisitDevices(DeviceScanActivity.this, true)) {
            		Toast.makeText(getApplicationContext(), "Error calling API.scanDephisitDevices() method", Toast.LENGTH_SHORT).show();
            	} else {
            		mScanning = true;
            		invalidateOptionsMenu();
                    Toast.makeText(getApplicationContext(), "Scanning Dephisit devices...", Toast.LENGTH_SHORT).show();
            	}
            	
				/*MESSENGER
				Message msg = Message.obtain(null, BluetoothLeService.DEPHISIT_SCAN_DEVICES);
		        msg.replyTo = new Messenger(new ResponseHandler());
				try {
					messenger.send(msg);
				} catch (RemoteException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}*/
		        
                break;
                
            case R.id.menu_stop:
            	/*DEPRECATED
            	try {
					if (!mBluetoothLeService.scanDephisitDevices(false)) {
	                	Toast.makeText(getApplicationContext(), "Error calling API.scanDephisitDevices() method", Toast.LENGTH_SHORT).show();
	            	} else {
	            		mScanning = false;
	            		invalidateOptionsMenu();
	                	//Toast.makeText(getApplicationContext(), "Dephisit devices scan stoped", Toast.LENGTH_SHORT).show();
	            	}
				} catch (DephisitException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
            	
            	if (!mBluetoothLeService.scanDephisitDevices(DeviceScanActivity.this, false)) {
                	Toast.makeText(getApplicationContext(), "Error calling API.scanDephisitDevices() method", Toast.LENGTH_SHORT).show();
            	} else {
            		mScanning = false;
            		invalidateOptionsMenu();
                	//Toast.makeText(getApplicationContext(), "Dephisit devices scan stoped", Toast.LENGTH_SHORT).show();
            	}
            	
                break;
        }
        return true;
    }
    
    @Deprecated
    public void onScanFinished() {
    	Log.d(TAG, "onScanFinished()");
    	
    	mScanning = false;
    	invalidateOptionsMenu();
    	//Toast.makeText(getApplicationContext(), "Dephisit devices scan finished", Toast.LENGTH_SHORT).show();
    }
    
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	Log.d(TAG, "onCreate()");
    	
        super.onCreate(savedInstanceState);
        getActionBar().setTitle(R.string.title_devices);
       
        mLeDeviceListAdapter = new LeDeviceListAdapter();
        setListAdapter(mLeDeviceListAdapter);
        
        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.
        BluetoothAdapter mBluetoothAdapter = ((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
        	// JESUS - Para conectar con el servicio DEPHISIT solo si el Bluetooth esta habilitado ---------------
            // Bind to LocalService
            Intent gattServiceIntent = new Intent(this, DephisitService.class);
            bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
            // JESUS ---------------------------------------------------------------------------------------------
        }
    }

    @Override
    protected void onResume() {
    	Log.d(TAG, "onResume()");
    	
        super.onResume();
    }

    @Override
    protected void onPause() {
    	Log.d(TAG, "onPause()");
    	
        super.onPause();
        
        if (mBound) {
        	/*DEPRECATED
        	try {
				mBluetoothLeService.scanDephisitDevices(false);
				mScanning = false;
				invalidateOptionsMenu();
			} catch (DephisitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
        	
        	mBluetoothLeService.scanDephisitDevices(DeviceScanActivity.this, false);
			mScanning = false;
			invalidateOptionsMenu();
        }
    }
    
    @Override
    protected void onStart() {
    	Log.d(TAG, "onStart()");
    	
        super.onStart();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        } else {
        	// JESUS - Para conectar con el servicio DEPHISIT solo si el Bluetooth esta habilitado ---------------
            // Bind to LocalService
            Intent gattServiceIntent = new Intent(this, DephisitService.class);
            bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
            // JESUS ---------------------------------------------------------------------------------------------
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    @Override
    protected void onStop() {
    	Log.d(TAG, "onStop()");
    	
        super.onStop();
    }

    @Override
    protected void onDestroy() {
    	Log.d(TAG, "onDestroy()");
    	
        super.onDestroy();
        
        // Unbind from the service
        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }
    }
    
    
    // Adapter for holding devices found through scanning.
    private class LeDeviceListAdapter extends BaseAdapter {
        private ArrayList<String> mLeDevices;
        private LayoutInflater mInflator;

        public LeDeviceListAdapter() {
            super();
            mLeDevices = new ArrayList<String>();
            mInflator = DeviceScanActivity.this.getLayoutInflater();
        }

        public void addDevice(String deviceId) {
            if(!mLeDevices.contains(deviceId)) {
                mLeDevices.add(deviceId);
            }
        }

        public void removeDevice(String deviceId) {
        	if(mLeDevices.contains(deviceId)) {
                mLeDevices.remove(deviceId);
            }
        }
        
        public String getDevice(int position) {
            return mLeDevices.get(position);
        }

        /*public void clear() {
            mLeDevices.clear();
        }*/

        @Override
        public int getCount() {
            return mLeDevices.size();
        }

        @Override
        public Object getItem(int i) {
            return mLeDevices.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            ViewHolder viewHolder;
            // General ListView optimization code.
            if (view == null) {
                view = mInflator.inflate(R.layout.listitem_device, null);
                viewHolder = new ViewHolder();
                viewHolder.deviceLocation = (TextView) view.findViewById(R.id.device_location);
                viewHolder.deviceType = (TextView) view.findViewById(R.id.device_type);
                viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }

            String deviceId = mLeDevices.get(i);
            
            String[] tln = deviceId.split(":");

            final String deviceName = tln[2];
            if (deviceName != null && deviceName.length() > 0)
                viewHolder.deviceName.setText(deviceName);
            else
                viewHolder.deviceName.setText(R.string.unknown_device);
            viewHolder.deviceType.setText("Type: " + DeviceType.valueOf(tln[0]).toString());
            viewHolder.deviceLocation.setText("Location: " + Location.fromCode(tln[1]).toString());

            return view;
        }
    }
    
    static class ViewHolder {
        TextView deviceName;
        TextView deviceType;
        TextView deviceLocation;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        final String deviceId = mLeDeviceListAdapter.getDevice(position);
        if (deviceId == null) return;
        
        Intent intent = null;
        
        Log.v(TAG, "Item clicked!");
        
        String[] tln = deviceId.split(":");
        DeviceType dt = DeviceType.valueOf(tln[0]);
        
        
        try {
        	intent = new Intent(this, Class.forName(getPackageName()+"."+dt.name()+"DeviceActivity"));
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        intent.putExtra("deviceId", deviceId);
        
        if (mScanning) {
        	/*DEPRECATED
        	try {
				if (!mBluetoothLeService.scanDephisitDevices(false)) {
                	Toast.makeText(getApplicationContext(), "Error calling API.scanDephisitDevices() method", Toast.LENGTH_SHORT).show();
            	} else {
            		mScanning = false;
            		invalidateOptionsMenu();
                	Toast.makeText(getApplicationContext(), "Dephisit devices scan stoped", Toast.LENGTH_SHORT).show();
            	}
			} catch (DephisitException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
        	
        	if (!mBluetoothLeService.scanDephisitDevices(DeviceScanActivity.this, false)) {
            	Toast.makeText(getApplicationContext(), "Error calling API.scanDephisitDevices() method", Toast.LENGTH_SHORT).show();
        	} else {
        		mScanning = false;
        		invalidateOptionsMenu();
            	Toast.makeText(getApplicationContext(), "Dephisit devices scan stoped", Toast.LENGTH_SHORT).show();
        	}
        }
        startActivity(intent);
    }

	@Override
	public void onDephisitDevice(final String deviceId, final boolean inRange) {
		Log.d(TAG, "onDephisitDevice() - deviceId:" + deviceId + " ,inRange:" + inRange);
    	
   	 	runOnUiThread(new Runnable() {
            @Override
            public void run() {
            	if (inRange) {
            		mLeDeviceListAdapter.addDevice(deviceId);
            	} else {
            		mLeDeviceListAdapter.removeDevice(deviceId);
            	}
                mLeDeviceListAdapter.notifyDataSetChanged();
            }
        });
	}
    
}