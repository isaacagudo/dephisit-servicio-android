/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * LSWNDeviceI - Interface to be implemented by LightSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface LSWNDeviceI extends DephisitDeviceI {

	boolean getLight(LSWNDeviceListener lswnDListener);
	
	boolean areNotificationsEnabled();
	boolean enableNotifications(LSWNDeviceListener lswnDListener);
	boolean disableNotifications(LSWNDeviceListener lswnDListener);
	
	int getThreshold();
	boolean setThreshold(int threshold);
}
