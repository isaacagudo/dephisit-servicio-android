/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * LADeviceActivity - Activity for use LightActuator devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class LADeviceActivity extends Activity implements DADeviceListener, DephisitCallback {

	private final static String TAG = LADeviceActivity.class.getSimpleName();
	
	private DephisitService mBluetoothLeService;
	
	private String deviceId;
	private LADeviceI device;

	private String currentState = null;
	
	private boolean mBound;
	
	// Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
        	mBluetoothLeService = ((DephisitService.LocalBinder) service).getService();
        	mBound = true;
        	
        	Log.i(TAG, "BluetoothLeService (API) connected");
        	
        	/*device = (LADeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
        	
        	runOnUiThread(new Runnable() {
    			public void run() {
    				((Button)findViewById(R.id.laSetLightState)).setEnabled(true);
    			}
    		});*/
        	
        	mBluetoothLeService.scanDephisitDevices(LADeviceActivity.this, true, deviceId);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        	mBluetoothLeService = null;
        	mBound = false;
        	
        	Log.i(TAG, "BluetoothLeService (API) disconnected");
        }
    };
	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ladevice);
		
		Log.d(TAG, "onCreate()");
		
		Intent intent = getIntent();
		
		deviceId = intent.getStringExtra("deviceId");

		Log.d(TAG, "onCreate() - deviceId:" + deviceId);
		
		((TextView)findViewById(R.id.laDeviceId)).setText(deviceId);
		
		Button button2 = (Button)findViewById(R.id.laSetLightState);
		button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	showDialog();
            }
        });
		button2.setEnabled(false);
	}
	
	private void showDialog()
    {
         final Dialog d = new Dialog(LADeviceActivity.this);
         d.setTitle("Set light state");
         d.setContentView(R.layout.spinner_dialog);
         Button b4 = (Button) d.findViewById(R.id.button4);
         Button b3 = (Button) d.findViewById(R.id.button3);
         final Spinner s = (Spinner) d.findViewById(R.id.spinner1);
         List<String> lightStates = new ArrayList<String>();
         lightStates.add("ON");
         lightStates.add("OFF");
         ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lightStates);
         adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
         s.setAdapter(adapter);
         s.setSelection(adapter.getPosition(currentState));
         
         b4.setOnClickListener(new OnClickListener()
         {
          @Override
          public void onClick(View v) {
              //tv.setText(String.valueOf(np.getValue())); //set the value to textview
        	  Toast.makeText(getApplicationContext(), "Value: " + s.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
        	  currentState = s.getSelectedItem().toString();
        	  device.setLightState("ON".equals(s.getSelectedItem().toString()));
              d.dismiss();
           }    
          });
         b3.setOnClickListener(new OnClickListener()
         {
          @Override
          public void onClick(View v) {
              d.dismiss(); // dismiss the dialog
           }    
          });
       d.show();
    }
	
	@Override
    protected void onStart() {
    	Log.d(TAG, "onStart()");
    	
        super.onStart();

        // Bind to LocalService
        Intent gattServiceIntent = new Intent(this, DephisitService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }
    
    @Override
    protected void onStop() {
    	Log.d(TAG, "onStop()");
    	
        super.onStop();
        
        // Unbind from the service
        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }
    }

	@Override
	public void onDephisitDevice(final String deviceId, final boolean available) {
		//if (device.getDeviceId().equals(deviceId)) {
    		runOnUiThread(new Runnable() {
				public void run() {
		    		if (available) {
		    			device = (LADeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
		    			
						((Button)findViewById(R.id.laSetLightState)).setEnabled(true);
						Toast.makeText(getApplicationContext(), "DEPHISIT device available", Toast.LENGTH_SHORT).show();
					} else {
						//device = null;
						
						((Button)findViewById(R.id.laSetLightState)).setEnabled(false);
						Toast.makeText(getApplicationContext(), "DEPHISIT device unavailable", Toast.LENGTH_SHORT).show();
					}
				}
    		});
    	//}
	}

	@Override
	@Deprecated
	public void onScanFinished() {
		// TODO Auto-generated method stub
		
	}
}
