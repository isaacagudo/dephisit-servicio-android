/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * SeatSWNDeviceListener - Interface to be implemented by clients that use SeatSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface SeatSWNDeviceListener {

	void onSeatStateValue(SeatSWNDeviceI seatswnDevice, boolean value);
	void onSeatStateChange(SeatSWNDeviceI seatswnDevice, boolean value);
	
}
