/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * RoadState - Weather road conditions detected.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public enum RoadState {
	UNKNOWN (0x00, "Unknown"),
    
	DRY (0x01, "Dry"),
    WET (0x02, "Wet"),
    ICE (0x03, "Ice"),
    SNOW (0x04, "Snow");
	
	
    private final int code;
    private final String name;
    
    RoadState(int code, String name) {
    	this.code = code;
    	this.name = name;
    }

	public int getCode() {
		return code;
	}
	
	public String toString(){
		return name;
	}
	
	static public RoadState fromCode(String code) {
		RoadState roadState = RoadState.UNKNOWN;
		for (RoadState rs : RoadState.values()) {
			if (rs.getCode() == Integer.valueOf(code).intValue()) {
				roadState = rs;
			}
		}
		return roadState;
	}
}
