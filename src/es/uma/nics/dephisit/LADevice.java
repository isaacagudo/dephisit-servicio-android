/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

/**
 * LADevice - Implementation of the LightActuator device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class LADevice extends DephisitDevice implements LADeviceI {

	private final static String TAG = LADevice.class.getSimpleName();
	
	private BluetoothGattCharacteristic lightValue = null;
	
	//TODO Use synchronization?
	//private Boolean free = true;
	//private final Object lock = new Object();
	
	private static enum LAChar {LA_LIGHT_VALUE}
	
	//Write attribute
	private static final AttMap[] LA_LIGHT_VALUE_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000007-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002222-0000-1000-8000-00805f9b34fb")
	};

	
	public LADevice(String n, Location l, BluetoothGatt bleDevice, DephisitService bluetoothLeService) {
		super(DeviceType.LA, n, l, bleDevice, bluetoothLeService);
	}
	
	@Override
	public boolean setLightState(boolean state) {
		Log.d(TAG, "setLightState() - state: " + state);
		
		DephisitService.setBooleanValueToCharacteristic(lightValue, state);
		return DephisitService.writeCharacteristic(bleDevice, lightValue);
	}
	
	static String isValidCharacteristic(BluetoothGattCharacteristic characteristic) {
		for (AttMap map : LA_LIGHT_VALUE_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {
				return LAChar.LA_LIGHT_VALUE.toString();
			}
		}
		return null;
	}

	@Override
	public boolean isComplete() {
		return this.lightValue!=null;
	}
	
	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		switch (LAChar.valueOf(charName)) {
		case LA_LIGHT_VALUE:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'LightValue' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.lightValue = characteristic;
			break;
		default:
			break;
		}
	}
}
