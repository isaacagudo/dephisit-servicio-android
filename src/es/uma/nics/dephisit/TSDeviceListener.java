/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * TSDeviceListener - Interface to be implemented by clients that use TemperatureSensor devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface TSDeviceListener {

    void onTempValue(TSDeviceI tsDevice, float value);
}

