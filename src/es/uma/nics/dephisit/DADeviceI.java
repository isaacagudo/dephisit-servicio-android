/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * DADeviceI - Interface to be implemented by DisplayActuator devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface DADeviceI extends DephisitDeviceI {

	boolean setMessage(String message);
}
