/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

/**
 * TSDevice - Implementation of the TemperatureSensor device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class TSDevice extends DephisitDevice implements TSDeviceI {

	private final static String TAG = TSDevice.class.getSimpleName();
	
	private BluetoothGattCharacteristic temperatureValue = null;
	
	private TSDeviceListener tsDListener = null;
	
	//TODO Use synchronization?
	//private Boolean free = true;
	//private final Object lock = new Object();
	
	private static enum TSChar {TS_TEMPERATURE_VALUE}
	
	//Read attribute
	private static final AttMap[] TS_TEMPERATURE_VALUE_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000011-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002221-0000-1000-8000-00805f9b34fb")
	};

	private final DephisitDeviceCallback callback = new DephisitDeviceCallback() {

		@Override
		public void onValue(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValue() - characteristic: " + characteristic.getUuid());
			
			if (characteristic==temperatureValue) {
				
				//synchronized(lock) {

				tsDListener.onTempValue(TSDevice.this, DephisitService.getFloatLEValueFromCharacteristic(characteristic));
				
				//free = true;
				//free.notify();
				//}
			}
		}
	};
	
	
	public TSDevice(String n, Location l, BluetoothGatt bleDevice, DephisitService bluetoothLeService) {
		super(DeviceType.TS, n, l, bleDevice, bluetoothLeService);
	}

	@Override
	public boolean getTemp(TSDeviceListener tsDListener) {
		Log.d(TAG, "getTemp() - tsDListener: " + tsDListener);
		
		/*synchronized(lock) {
			if (!free) {
				try {
					free.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			this.tsDListener = tsDListener;
			free = false;
			
			return bluetoothLeService.readCharacteristic(bleDevice, temperatureValue, callback);
		}*/
		this.tsDListener = tsDListener;
		
		return DephisitService.readCharacteristic(bleDevice, temperatureValue, callback);
	}
	
	static String isValidCharacteristic(BluetoothGattCharacteristic characteristic) {
		for (AttMap map : TS_TEMPERATURE_VALUE_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
				return TSChar.TS_TEMPERATURE_VALUE.toString();
			}
		}
		return null;
	}

	@Override
	public boolean isComplete() {
		return this.temperatureValue!=null;
	}
	
	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		switch (TSChar.valueOf(charName)) {
		case TS_TEMPERATURE_VALUE:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'TemperatureValue' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.temperatureValue = characteristic;
			break;
		default:
			break;
		}
	}
}
