/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * PSWNDeviceListener - Interface to be implemented by clients that use PositionSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface PSWNDeviceListener {

	void onPositionValue(PSWNDeviceI pswnDevice, float latitude, float longitude);
	void onPositionChange(PSWNDeviceI pswnDevice, float latitude, float longitude);
}
