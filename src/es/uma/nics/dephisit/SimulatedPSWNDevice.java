/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;


//TODO Synchronize calls?

/**
 * SimulatedPSWNDevice - Implementation of a simulated PositionSensorWithNotifications device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class SimulatedPSWNDevice extends SimulatedDephisitDevice implements PSWNDeviceI {
	private final static String TAG = SimulatedPSWNDevice.class.getSimpleName();
	
	private PSWNDeviceListener pswnDListener = null;
	
	private float previousLongitude, previousLatitude;
	private float currentLongitude = SimulationData.SIMULATED_COORDINATES[0][0], 
			currentLatitude = SimulationData.SIMULATED_COORDINATES[0][1];
	
	private float positionThreshold = 0.0f;
	
	Thread simulatedPSWNThread = new Thread(new Runnable() {
        public void run() {

        	while (true) {
            	for (int i=0; i<SimulationData.SIMULATED_COORDINATES.length; i++) {

            		try {
            			//Wait TIME_INTERVAL miliseconds between each step
						Thread.sleep(SimulationData.TIME_INTERVAL);
					
	            		//Obtain the new position
	            		previousLongitude = currentLongitude;
	            		previousLatitude = currentLatitude;
	            		currentLongitude = SimulationData.SIMULATED_COORDINATES[i][0];
	            		currentLatitude = SimulationData.SIMULATED_COORDINATES[i][1];
	            		if (((Math.abs(currentLongitude-previousLongitude) > positionThreshold) 
	            				|| (Math.abs(currentLatitude-previousLatitude) > positionThreshold)) && simulatedNotifications) {
	            			//pswnDListener.onPositionChange(PSWNDevice.this, currentLongitude, currentLatitude);
	            			callback.onValueChange(null);
	            		}
            		
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
            	}
            }
        }
    });

	private boolean simulatedNotifications = false;
	
	private final DephisitDeviceCallback callback = new DephisitDeviceCallback() {

		@Override
		public void onValue(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValue()");
			
			pswnDListener.onPositionValue(SimulatedPSWNDevice.this, currentLatitude, currentLongitude);
		}
		
		@Override
		public void onValueChange(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValueChange()");

			pswnDListener.onPositionChange(SimulatedPSWNDevice.this, currentLatitude, currentLongitude);
		}
	};
	
	
	public SimulatedPSWNDevice(String n, Location l) {
		super(DeviceType.PSWN, n, l);
		
		//Simulate this device 
		simulatedPSWNThread.start();
	}

	@Override
	public boolean getPosition(PSWNDeviceListener pswnDListener) {
		Log.d(TAG, "getPosition() - pswnDListener:" + pswnDListener);

		this.pswnDListener = pswnDListener;
		
		//pswnDListener.onPositionValue(GPSWNDevice.this, currentLongitude, currentLatitude);
		callback.onValue(null);
		
		return true;
	}
	
	//TODO Change for use multiple clients???
	@Override
	public boolean areNotificationsEnabled() {
		//return this.pswnDListener!=null;
		return this.simulatedNotifications;
	}
	
	@Override
	public boolean enableNotifications(PSWNDeviceListener pswnDListener) {
		this.pswnDListener = pswnDListener;
		this.simulatedNotifications = true;
		
		return true;
	}
	
	@Override
	public boolean disableNotifications(PSWNDeviceListener pswnDListener) {
		this.pswnDListener = null;
		this.simulatedNotifications = false;
		
		return true;
	}
	
	@Override
	public float getThreshold() {
		return this.positionThreshold;
	}
	
	@Override
	public boolean setThreshold(float threshold) {
		this.positionThreshold = threshold;
		
		return true;
	}
}
