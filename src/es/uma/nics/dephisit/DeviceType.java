/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * DeviceType - DEPHISIT types.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public enum DeviceType {
    // Devices currently used in DEPHISIT platform //
	
	// Sensor devices //
    PSWN,
    SSWN,
    SeatSWN,
    
    // Actuator devices //
    DA,
    LA,
    SA,

    // Detector devices //
    WD,
    BD,
    TSD,
    
    // Devices NOT currently used in DEPHISIT platform (defined for future use) //
    TS,
    TSWN,
    LS,
    LSWN;
	
    public String toString(){
		switch (this) {
			case PSWN: return "Position Sensor With Notifications";
			case SSWN: return "Speed Sensor With Notifications";
			case SeatSWN: return "Seat Sensor With Notifications";
			
			case DA: return "Display Actuator";
			case LA: return "Light Actuator";
			case SA: return "Sound Actuator";

			case WD: return "Weather Detector";
			case BD: return "Bicycle Detector";
			case TSD: return "Traffic Signals Detector";
			
			case TS: return "Temperature Sensor";
			case TSWN: return "Temperature Sensor With Notifications";
			case LS: return "Light Sensor";
			case LSWN: return "Light Sensor With Notifications";
		}
		
		return "<Unknown device type>";
	}
}
