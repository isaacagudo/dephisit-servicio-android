/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * Location - DEPHISIT locations.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public enum Location {
		UNKNOWN (0, "Unknown"),
		
	    ROOF (1, "Roof"),
	    SUN_ROOF (2, "Sun roof"),
	    
	    WINDSHIELD (3, "Windshield"),
	    LEFT_FRONT_WINDOW (4, "Left front window"),
	    RIGHT_FRONT_WINDOW (5, "Right front window"),
	    LEFT_REAR_WINDOW (6, "Left rear window"),
	    RIGHT_REAR_WINDOW (7, "Right front window"),
	    REAR_WINDOW (8, "Rear window"),
	    
	    LEFT_FRONT_DOOR (9, "Left front door"),
	    RIGHT_FRONT_DOOR (10, "Right front door"),
	    LEFT_REAR_DOOR (11, "Left rear door"),
	    RIGHT_REAR_DOOR (12, "Right rear door"),
	    
	    LEFT_OUTSIDE_MIRROR (13, "Left outside mirror"),
	    RIGHT_OUTSIDE_MIRROR (14, "Right outside mirror"),
	    
	    HOOD (15, "Hood"),
	    TRUNK (16, "Trunk"),
	    
	    FRONT_BUMPER (17, "Front bumper"),
	    REAR_BUMPER (18, "Rear bumper"),
	    
	    LEFT_HEAD_LIGHT (19, "Left head light"),
	    RIGHT_HEAD_LIGHT (20, "Right head light"),
	    LEFT_TAIL_LIGHT (21, "Left tail light"),
	    RIGTH_TAIL_LIGHT (22, "Right tail light"),
	    CENTER_TAIL_LIGHT (23, "Center tail light"),
	    
	    LEFT_FRONT_INDICATOR_LIGHT (24, "Left front indicator light"),
	    RIGHT_FRONT_INDICATOR_LIGHT (25, "Right front indicator light"),
	    LEFT_REAR_INDICATOR_LIGHT (26, "Left rear indicator light"),
	    RIGHT_REAR_INDICATOR_LIGHT (27, "Right rear indicator light"),
	    
	    LEFT_FRONT_WHEEL (28, "Left front wheel"),
	    RIGHT_FRONT_WHEEL (29, "Right front wheel"),
	    LEFT_REAR_WHEEL (30, "Left rear wheel"),
	    RIGHT_REAR_WHEEL (31, "Right rear wheel"),
	    
	    LEFT_FRONT_SEAT (32, "Left front seat"),
	    RIGHT_FRONT_SEAT (33, "Right front seat"),
	    LEFT_REAR_SEAT (34, "Left rear seat"),
	    RIGTH_REAR_SEAT (35, "Right rear seat"),
	    CENTER_REAR_SEAT (36, "Center rear seat"),
	    
	    REARVIEW_MIRROR (37, "Rearview mirror"),
	    STERRING_WHEEL (38, "Sterring wheel"),
	    AIR_FILTER (39, "Air filter"),
	    BATTERY (40, "Battery"),
	    DISTRIBUTOR (41, "Distributor"),
	    RADIATOR (42, "Radiator"),
	    ALTERNATOR (43, "Alternator"),
	    OIL_FILTER (44, "Oil filter"),
	    ENGINE (45, "Engine"),
	    
	    LEFT_FRONT_BRAKE (46, "Left front brake"),
	    RIGHT_FRONT_BRAKE (47, "Right front brake"),
	    LEFT_REAR_BRAKE (48, "Left rear brake"),
	    RIGHT_REAR_BRAKE (49, "Right rear brake"),
	    
	    DASHBOARD (50, "Dashboard"),
	    INSTRUMENTS_CLUSTER (51, "Instruments cluster");
	    
	    private final int code;
	    private final String name;
	    
	    Location(int code, String name) {
	    	this.code = code;
	    	this.name = name;
	    }

		public int getCode() {
			return code;
		}
		
		public String toString(){
			return name;
		}
		
		static public Location fromCode(String code) {
			Location location = Location.UNKNOWN;
			for (Location l : Location.values()) {
				if (l.getCode() == Integer.valueOf(code).intValue()) {
					location = l;
				}
			}
			return location;
		}
}
