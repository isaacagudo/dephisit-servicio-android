/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * WDDeviceI - Interface to be implemented by WeatherDetector devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface WDDeviceI extends DephisitDeviceI {

	boolean areNotificationsEnabled();
	boolean enableNotifications(WDDeviceListener weatherDetectorDListener);
	boolean disableNotifications(WDDeviceListener weatherDetectorDListener);
}
