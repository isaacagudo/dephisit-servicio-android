/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;


//TODO Synchronize calls?

/**
 * SimulatedSSWNDevice - Implementation of a simulated SpeedSensorWithNotifications device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class SimulatedSSWNDevice extends SimulatedDephisitDevice implements SSWNDeviceI {
	private final static String TAG = SimulatedSSWNDevice.class.getSimpleName();
	
	private SSWNDeviceListener speedswnDListener = null;
	
	private int previousSpeed;
	private int currentSpeed = 0;
	
	private int speedThreshold = 0;
	
	Thread simulatedSpeedThread = new Thread(new Runnable() {
        public void run() {
        	float[] previousPosition;
        	float[] currentPosition = SimulationData.SIMULATED_COORDINATES[0];
        	
        	while (true) {
        		for (int i=0; i<SimulationData.SIMULATED_COORDINATES.length; i++) {

        			try {
            			//Wait TIME_INTERVAL miliseconds between each step
						Thread.sleep(SimulationData.TIME_INTERVAL);
						
	    				previousPosition = currentPosition;
						currentPosition = SimulationData.SIMULATED_COORDINATES[i];
	 
						float[] distance = new float[3];
	            		android.location.Location.distanceBetween(previousPosition[1], previousPosition[0], currentPosition[1], currentPosition[0], distance);
	            		
	            		float mps = distance[0] / (SimulationData.TIME_INTERVAL / 1000);
	            		int kmph = (int) (mps * 3600/1000);
	            		
	            		previousSpeed = currentSpeed;
	            		currentSpeed = kmph;
	            		
	            		if ((Math.abs(currentSpeed-previousSpeed) > speedThreshold) && simulatedNotifications) {
	            			//speedswnDListener.onPositionChange(SpeedSWNDevice.this, currentSpeed);
	            			callback.onValueChange(null);
	            		}
	            		
        			} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
        		}
            }
        }
    });

	private boolean simulatedNotifications = false;
	
	private final DephisitDeviceCallback callback = new DephisitDeviceCallback() {

		@Override
		public void onValue(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValue()");
			
			speedswnDListener.onSpeedValue(SimulatedSSWNDevice.this, currentSpeed);
		}
		
		@Override
		public void onValueChange(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValueChange()");

			speedswnDListener.onSpeedChange(SimulatedSSWNDevice.this, currentSpeed);
		}
	};
	
	
	public SimulatedSSWNDevice(String n, Location l) {
		super(DeviceType.SSWN, n, l);
		
		//Simulate this device 
		simulatedSpeedThread.start();
	}
	
	@Override
	public boolean getSpeed(SSWNDeviceListener speedswnDListener) {
		Log.d(TAG, "getSpeed() - speedswnDListener:" + speedswnDListener);

		this.speedswnDListener = speedswnDListener;
		
		//speedswnDListener.onPositionValue(SpeedSWNDevice.this, currentSpeed);
		callback.onValue(null);
		
		return true;
	}
	
	//TODO Change for use multiple clients???
	@Override
	public boolean areNotificationsEnabled() {
		//return this.speedswnDListener!=null;
		return this.simulatedNotifications;
	}
	
	@Override
	public boolean enableNotifications(SSWNDeviceListener speedswnDListener) {
		this.speedswnDListener = speedswnDListener;
		this.simulatedNotifications = true;
		
		return true;
	}
	
	@Override
	public boolean disableNotifications(SSWNDeviceListener speedswnDListener) {
		this.speedswnDListener = null;
		this.simulatedNotifications = false;
		
		return true;
	}
	
	@Override
	public int getThreshold() {
		return this.speedThreshold;
	}
	
	@Override
	public boolean setThreshold(int threshold) {
		this.speedThreshold = threshold;
		
		return true;
	}
}
