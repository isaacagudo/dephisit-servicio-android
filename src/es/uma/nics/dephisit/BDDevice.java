/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;


//TODO Synchronize calls?
//TODO Create a common interface for this type of devices (detectors)

/**
 * BDDevice - Implementation of the BicycleDetector device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class BDDevice extends DephisitDevice implements BDDeviceI {
	private final static String TAG = BDDevice.class.getSimpleName();
	
	private BDDeviceListener bicycleDetectorDListener = null;
	
	private volatile Map<String, BBeacon> bBeaconsTime = new HashMap<String, BBeacon>();
	
	//private final Object lock = new Object();
	
	class BBeacon {
		protected float latitude;
		protected float longitude;
		protected byte bicycleState;
		protected long lastTime;
		
		protected BBeacon(float latitude, float longitude, byte bicycleState, long lastTime) {
			this.latitude = latitude;
			this.longitude = longitude;
			this.bicycleState = bicycleState;
			this.lastTime = lastTime;
		}
	}
	
	Thread bdThread = new Thread(new Runnable() {
        public void run() {
        	
        	List<String> keysToDelete = new ArrayList<String>();
        	
        	while (true) {
        		//synchronized (lock) {
        		for (Entry<String, BBeacon> entryBeacon: bBeaconsTime.entrySet()) {
        			BBeacon bBeacon = entryBeacon.getValue();
        			String bBeaconId = entryBeacon.getKey();
        			if (System.currentTimeMillis() - 2000 > bBeacon.lastTime) {
        				if (bicycleDetectorDListener != null) {
        					bicycleDetectorDListener.onBicycleWarningOutOfRange(BicycleState.fromCode(String.valueOf(bBeacon.bicycleState)),
        							bBeacon.latitude, bBeacon.longitude);
        				}
        				
        				//Delete traffic sign beacon
        				keysToDelete.add(bBeaconId);
        			}
        		}
        		
        		for (String key: keysToDelete) {
        			bBeaconsTime.remove(key);
        		}
        		keysToDelete.clear();
        		//}
        		
        		try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
            }
        }
    });
	

	public BDDevice(String n, Location l) {
		super(DeviceType.BD, n, l, null, null);
		
		bdThread.start();
	}
	
	//TODO Change for use multiple clients???
	@Override
	public boolean areNotificationsEnabled() {
		return this.bicycleDetectorDListener!=null;
	}
	
	@Override
	public boolean enableNotifications(BDDeviceListener bicycleDetectorDListener) {
		this.bicycleDetectorDListener = bicycleDetectorDListener;
		
		//synchronized (lock) {
		for (BBeacon bBeacon: bBeaconsTime.values()) {
			bicycleDetectorDListener.onNewBicycleWarningInRange(BicycleState.fromCode(String.valueOf(bBeacon.bicycleState)),
					bBeacon.latitude, bBeacon.longitude);
		//}
		}
		
		return true;
	}
	
	@Override
	public boolean disableNotifications(BDDeviceListener bicycleDetectorDListener) {
		this.bicycleDetectorDListener = null;
		
		return true;
	}
	
	@Override
	public boolean isComplete() {
		return true;
	}

	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		//Do nothing, this is a detector device
	}
	
	void notifyAdvertisement(String id, float latitude, float longitude, byte bicycleState) {
		Log.d(TAG, "Bicycle Advertisement - id: " + id + ", latitude: " + latitude + ", longitude: " + longitude 
				+ "bicycleState: " + bicycleState);
		
		BBeacon bBeacon = new BBeacon(latitude, longitude, bicycleState, System.currentTimeMillis());
	
		if (BicycleState.fromCode(String.valueOf(bicycleState)) != BicycleState.STOPPED) {
		
		//synchronized (lock) {
		if (bBeaconsTime.containsKey(id) && bBeaconsTime.get(id).bicycleState == bicycleState) {
			bBeaconsTime.put(id, bBeacon);
			
		} else {
			//Put state in the map
			bBeaconsTime.put(id, bBeacon);

			//Notify state to listener
			if (bicycleDetectorDListener != null) {
				bicycleDetectorDListener.onNewBicycleWarningInRange(BicycleState.fromCode(String.valueOf(bicycleState)),
						bBeacon.latitude, bBeacon.longitude);
			}
		}
		//}
		
		}
	}
}
