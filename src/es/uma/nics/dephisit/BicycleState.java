/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * BicycleState - State of the bicycle detected.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public enum BicycleState {
	UNKNOWN (0x00, "Unknown"),
    
	STOPPED (0x01, "Stopped"),
	ONGOING (0x02, "Ongoing"),
    ACCIDENT (0x03, "Accident");
	
	
    private final int code;
    private final String name;
    
    BicycleState(int code, String name) {
    	this.code = code;
    	this.name = name;
    }

	public int getCode() {
		return code;
	}
	
	public String toString(){
		return name;
	}
	
	static public BicycleState fromCode(String code) {
		BicycleState bicycleState = BicycleState.UNKNOWN;
		for (BicycleState bs : BicycleState.values()) {
			if (bs.getCode() == Integer.valueOf(code).intValue()) {
				bicycleState = bs;
			}
		}
		return bicycleState;
	}
}
