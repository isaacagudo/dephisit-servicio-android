/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGattCharacteristic;

/**
 * DephisitDeviceCallback - Abstract class that define the callback methods to be implemented by DEPHISIT devices 
 * in order to receive the values read from BLE characteristics.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public abstract class DephisitDeviceCallback {

	public void onValue(BluetoothGattCharacteristic characteristic) { }

	public void onValueChange(BluetoothGattCharacteristic characteristic) { }
}
