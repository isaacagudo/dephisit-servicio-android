/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * TSDDeviceListener - Interface to be implemented by clients that use TrafficSignalsDetector devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface TSDDeviceListener {

	void onNewTrafficSignInRange(TrafficSign type, float latitude, float longitude, int fromDirection, int toDirection);
	void onTrafficSignOutOfRange(TrafficSign type, float latitude, float longitude, int fromDirection, int toDirection);
}
