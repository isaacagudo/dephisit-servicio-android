/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * SADeviceListener - Interface to be implemented by clients that use SoundActuator devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface SADeviceListener {

}

