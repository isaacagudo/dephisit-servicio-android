/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

/**
 * PSWNDeviceActivity - Activity for use PositionSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class PSWNDeviceActivity extends Activity implements PSWNDeviceListener, DephisitCallback {

	private final static String TAG = PSWNDeviceActivity.class.getSimpleName();
	
	private DephisitService mBluetoothLeService;
	
	private String deviceId;
	private PSWNDeviceI device;

	private int currentThresholdIndex = 0;
	
	private boolean mBound;
	
	// Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
        	mBluetoothLeService = ((DephisitService.LocalBinder) service).getService();
        	mBound = true;
        	
        	Log.i(TAG, "BluetoothLeService (API) connected");
        	
        	/*device = (PSWNDeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
        	
        	runOnUiThread(new Runnable() {
    			public void run() {
    				((Button)findViewById(R.id.pswnGetPosition)).setEnabled(true);
    				((Button)findViewById(R.id.pswnSetThreshold)).setEnabled(true);
    				
    				//((Switch)findViewById(R.id.pswnNotifications)).setChecked(device.areNotificationsEnabled());
    				((Switch)findViewById(R.id.pswnNotifications)).setEnabled(true);
    			}
    		});*/
        	
        	mBluetoothLeService.scanDephisitDevices(PSWNDeviceActivity.this, true, deviceId);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        	mBluetoothLeService = null;
        	mBound = false;
        	
        	Log.i(TAG, "BluetoothLeService (API) disconnected");
        }
    };
	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pswndevice);
		
		Log.d(TAG, "onCreate()");
		
		Intent intent = getIntent();
		
		deviceId = intent.getStringExtra("deviceId");

		Log.d(TAG, "onCreate() - deviceId:" + deviceId);
		
		((TextView)findViewById(R.id.pswnDeviceId)).setText(deviceId);
		
		Button button = (Button)findViewById(R.id.pswnGetPosition);
		button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	device.getPosition(PSWNDeviceActivity.this);
            }
        });
		button.setEnabled(false);
		
		final Switch sw = (Switch)findViewById(R.id.pswnNotifications);
		sw.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		   @Override
		   public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		 
			   if (sw.isChecked()) {
		    		Toast.makeText(getApplicationContext(), "Notifications enabled", Toast.LENGTH_SHORT).show();
		    		device.enableNotifications(PSWNDeviceActivity.this);
		    	} else {
		    		Toast.makeText(getApplicationContext(), "Notifications disabled", Toast.LENGTH_SHORT).show();
		    		device.disableNotifications(PSWNDeviceActivity.this);
		    	}
		 
		   }
		});
		sw.setEnabled(false);
		
		Button button2 = (Button)findViewById(R.id.pswnSetThreshold);
		button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	showDialog();
            }
        });
		button2.setEnabled(false);
	}
	
	private void showDialog()
    {
         final Dialog d = new Dialog(PSWNDeviceActivity.this);
         d.setTitle("Choose threshold");
         d.setContentView(R.layout.dialog);
         Button b4 = (Button) d.findViewById(R.id.button4);
         Button b3 = (Button) d.findViewById(R.id.button3);
         final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
         final String[] nums = {"0.0000001","0.000001","0.00001","0.0001","0.001","0.01","0.1","1.0"};
         np.setMaxValue(nums.length-1);
         np.setMinValue(0);
         np.setDisplayedValues(nums);
         np.setWrapSelectorWheel(false);
         np.setValue(currentThresholdIndex);
         //np.setOnValueChangedListener(this);
         b4.setOnClickListener(new OnClickListener()
         {
          @Override
          public void onClick(View v) {
              //tv.setText(String.valueOf(np.getValue())); //set the value to textview
        	  Toast.makeText(getApplicationContext(), "Value: " + Float.parseFloat(nums[np.getValue()]), Toast.LENGTH_SHORT).show();
        	  currentThresholdIndex = np.getValue();
        	  device.setThreshold(Float.parseFloat(nums[currentThresholdIndex]));
              d.dismiss();
           }    
          });
         b3.setOnClickListener(new OnClickListener()
         {
          @Override
          public void onClick(View v) {
              d.dismiss(); // dismiss the dialog
           }    
          });
       d.show();
    }
	
	@Override
    protected void onStart() {
    	Log.d(TAG, "onStart()");
    	
        super.onStart();

        // Bind to LocalService
        Intent gattServiceIntent = new Intent(this, DephisitService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }
    
    @Override
    protected void onStop() {
    	Log.d(TAG, "onStop()");
    	
        super.onStop();
        
        device.disableNotifications(this);
        
        // Unbind from the service
        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }
    }

    public void onNotificationsClicked(View v)
    {
    	Switch sw = (Switch)v;
    	
    	if (sw.isChecked()) {
    		Toast.makeText(getApplicationContext(), "Notifications enabled", Toast.LENGTH_SHORT).show();
    		device.enableNotifications(this);
    	} else {
    		Toast.makeText(getApplicationContext(), "Notifications disabled", Toast.LENGTH_SHORT).show();
    		device.disableNotifications(this);
    	}
    }

	@Override
	public void onPositionValue(PSWNDeviceI pswnDevice, final float latitude, final float longitude) {
		runOnUiThread(new Runnable() {
			public void run() {
				((TextView)findViewById(R.id.pswnPositionValue)).setText(latitude+"�\n"+longitude+"�");
			}
		});
	}

	@Override
	public void onPositionChange(PSWNDeviceI pswnDevice, final float latitude, final float longitude) {
		runOnUiThread(new Runnable() {
			public void run() {
				((TextView)findViewById(R.id.pswnPositionValue)).setText(latitude+"�\n"+longitude+"�");
			}
		});
	}

	@Override
	public void onDephisitDevice(final String deviceId, final boolean available) {
		//if (device.getDeviceId().equals(deviceId)) {
			runOnUiThread(new Runnable() {
				public void run() {
					if (available) {
						device = (PSWNDeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
						
						((Button)findViewById(R.id.pswnGetPosition)).setEnabled(true);
						((Button)findViewById(R.id.pswnSetThreshold)).setEnabled(true);
						((Switch)findViewById(R.id.pswnNotifications)).setEnabled(true);
						Toast.makeText(getApplicationContext(), "DEPHISIT device available", Toast.LENGTH_SHORT).show();
					} else {
						//device = null;
						
						((Button)findViewById(R.id.pswnGetPosition)).setEnabled(false);
						((Button)findViewById(R.id.pswnSetThreshold)).setEnabled(false);
						((Switch)findViewById(R.id.pswnNotifications)).setEnabled(false);
						Toast.makeText(getApplicationContext(), "DEPHISIT device unavailable", Toast.LENGTH_SHORT).show();
					}
				}
			});
		//}
	}

	@Override
	@Deprecated
	public void onScanFinished() {
		// TODO Auto-generated method stub
		
	}
}
