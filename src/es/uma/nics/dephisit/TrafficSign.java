/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * TrafficSign - Type of traffic sign detected.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public enum TrafficSign {
	UNKNOWN (0x00, "Unknown"),
    
	TRAFFIC_LIGHT_GREEN (0x01, "Traffic Light - Green"),
    TRAFFIC_LIGHT_YELLOW (0x02, "Traffic Light - Yellow"),
    TRAFFIC_LIGHT_RED (0x03, "Traffic Light - Red"),
    TRAFFIC_LIGHT_FLASHING_YELLOW (0x04, "Traffic Light - Flashing Yellow"),
    TRAFFIC_LIGHT_FLASHING_RED (0x05, "Traffic Light - Flashing Red"),
    TRAFFIC_LIGHT_OFF (0x06, "Traffic Light - Off"),
    TRAFFIC_LIGHT_UNKNOWN (0x0F, "Traffic Light - Unknown"),
    
	STOP_SIGN (0x10, "Stop Sign");
	
	
    private final int code;
    private final String name;
    
    TrafficSign(int code, String name) {
    	this.code = code;
    	this.name = name;
    }

	public int getCode() {
		return code;
	}
	
	public String toString(){
		return name;
	}
	
	static public TrafficSign fromCode(String code) {
		TrafficSign trafficSign = TrafficSign.UNKNOWN;
		for (TrafficSign ts : TrafficSign.values()) {
			if (ts.getCode() == Integer.valueOf(code).intValue()) {
				trafficSign = ts;
			}
		}
		return trafficSign;
	}
}
