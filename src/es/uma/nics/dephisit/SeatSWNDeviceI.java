/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * SeatSWNDeviceI - Interface to be implemented by SeatSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface SeatSWNDeviceI extends DephisitDeviceI {

	boolean getSeatState(SeatSWNDeviceListener seatswnDListener);
	
	boolean areNotificationsEnabled();
	boolean enableNotifications(SeatSWNDeviceListener seatswnDListener);
	boolean disableNotifications(SeatSWNDeviceListener seatswnDListener);
}
