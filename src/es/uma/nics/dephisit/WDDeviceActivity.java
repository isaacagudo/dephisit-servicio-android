/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

/**
 * WDDeviceActivity - Activity for use WeatherDetector devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class WDDeviceActivity extends Activity implements WDDeviceListener, DephisitCallback {

	private final static String TAG = WDDeviceActivity.class.getSimpleName();
	
	private DephisitService mBluetoothLeService;
	
	private String deviceId;
	private WDDeviceI device;

	private boolean mBound;
	
	// Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
        	mBluetoothLeService = ((DephisitService.LocalBinder) service).getService();
        	mBound = true;
        	
        	Log.i(TAG, "BluetoothLeService (API) connected");
        	
        	/*device = (WDDeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
        	
        	runOnUiThread(new Runnable() {
    			public void run() {
    				((Switch)findViewById(R.id.wdNotifications)).setEnabled(true);
    			}
    		});*/
        	
        	mBluetoothLeService.scanDephisitDevices(WDDeviceActivity.this, true, deviceId);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        	mBluetoothLeService = null;
        	mBound = false;
        	
        	Log.i(TAG, "BluetoothLeService (API) disconnected");
        }
    };
	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_wddevice);
		
		Log.d(TAG, "onCreate()");
		
		Intent intent = getIntent();
		
		deviceId = intent.getStringExtra("deviceId");

		Log.d(TAG, "onCreate() - deviceId:" + deviceId);
		
		((TextView)findViewById(R.id.wdDeviceId)).setText(deviceId);
		
		final Switch sw = (Switch)findViewById(R.id.wdNotifications);
		sw.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		   @Override
		   public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		 
			   if (sw.isChecked()) {
		    		Toast.makeText(getApplicationContext(), "Notifications enabled", Toast.LENGTH_SHORT).show();
		    		device.enableNotifications(WDDeviceActivity.this);
		    	} else {
		    		Toast.makeText(getApplicationContext(), "Notifications disabled", Toast.LENGTH_SHORT).show();
		    		device.disableNotifications(WDDeviceActivity.this);
		    	}
		 
		   }
		});
		sw.setEnabled(false);
		
		// Bind to LocalService
        Intent gattServiceIntent = new Intent(this, DephisitService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
	}
	
	@Override
    protected void onStart() {
    	Log.d(TAG, "onStart()");
    	
        super.onStart();
    }
    
    @Override
    protected void onStop() {
    	Log.d(TAG, "onStop()");
    	
        super.onStop();
    }

    
    @Override
    protected void onDestroy() {
    	Log.d(TAG, "onDestroy()");
    	
        super.onDestroy();
        
        device.disableNotifications(this);
        
        // Unbind from the service
        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }
    }

    public void onNotificationsClicked(View v)
    {
    	Switch sw = (Switch)v;
    	
    	if (sw.isChecked()) {
    		Toast.makeText(getApplicationContext(), "Notifications enabled", Toast.LENGTH_SHORT).show();
    		device.enableNotifications(this);
    	} else {
    		Toast.makeText(getApplicationContext(), "Notifications disabled", Toast.LENGTH_SHORT).show();
    		device.disableNotifications(this);
    	}
    }
	
	@Override
	public void onNewWeatherWarningInRange(final RoadState type, final float latitude, final float longitude) {
		runOnUiThread(new Runnable() {
			public void run() {
				if (Float.isNaN(latitude) || Float.isNaN(longitude)) {
					((TextView)findViewById(R.id.wdValue)).setText("Beacon position not available");
				} else {
					((TextView)findViewById(R.id.wdValue)).setText(latitude+"�\n"+longitude+"�");
				}		
				//Put traffic signal's image
				int resource = 0;
				switch (type) {
					case WET: resource = R.drawable.wet; break;
					case ICE: resource = R.drawable.ice; break;
					case SNOW: resource = R.drawable.snow; break;
					default: break;
				}
				((ImageView)findViewById(R.id.wdImage)).setBackgroundResource(resource);
			}
		});
	}
	
	@Override
	public void onWeatherWarningOutOfRange(final RoadState type, final float latitude, final float longitude) {
		runOnUiThread(new Runnable() {
			public void run() {	
				((TextView)findViewById(R.id.wdValue)).setText("Out Of Range");
				//Put traffic signal's image
				int resource = 0;
				switch (type) {
				case WET: resource = R.drawable.wet; break;
				case ICE: resource = R.drawable.ice; break;
				case SNOW: resource = R.drawable.snow; break;
					default: break;
				}
				((ImageView)findViewById(R.id.wdImage)).setBackgroundResource(resource);
			}
		});
	}

	@Override
	public void onDephisitDevice(final String deviceId, final boolean available) {
		//if (device.getDeviceId().equals(deviceId)) {
			runOnUiThread(new Runnable() {
				public void run() {
					if (available) {
						device = (WDDeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
						
						((Switch)findViewById(R.id.wdNotifications)).setEnabled(true);
						Toast.makeText(getApplicationContext(), "DEPHISIT device available", Toast.LENGTH_SHORT).show();
					} else {
						//device = null;
						
						((Switch)findViewById(R.id.wdNotifications)).setEnabled(false);
						Toast.makeText(getApplicationContext(), "DEPHISIT device unavailable", Toast.LENGTH_SHORT).show();
					}
				}
			});
		//}
	}

	@Override
	@Deprecated
	public void onScanFinished() {
		// TODO Auto-generated method stub
		
	}
}
