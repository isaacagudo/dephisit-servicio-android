/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

/**
 * SSWNDeviceActivity - Activity for use SpeedSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class SSWNDeviceActivity extends Activity implements SSWNDeviceListener, DephisitCallback {

	private final static String TAG = SSWNDeviceActivity.class.getSimpleName();
	
	private DephisitService mBluetoothLeService;
	
	private String deviceId;
	private SSWNDeviceI device;

	private int currentThreshold = 1;
	
	private boolean mBound;
	
	// Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
        	mBluetoothLeService = ((DephisitService.LocalBinder) service).getService();
        	mBound = true;
        	
        	Log.i(TAG, "BluetoothLeService (API) connected");
        	
        	/*device = (SSWNDeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
        	
        	runOnUiThread(new Runnable() {
    			public void run() {
    				((Button)findViewById(R.id.sswnGetSpeed)).setEnabled(true);
    				((Button)findViewById(R.id.sswnSetThreshold)).setEnabled(true);
    				
    				//((Switch)findViewById(R.id.sswnNotifications)).setChecked(device.areNotificationsEnabled());
    				((Switch)findViewById(R.id.sswnNotifications)).setEnabled(true);
    			}
    		});*/
        	
        	mBluetoothLeService.scanDephisitDevices(SSWNDeviceActivity.this, true, deviceId);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        	mBluetoothLeService = null;
        	mBound = false;
        	
        	Log.i(TAG, "BluetoothLeService (API) disconnected");
        }
    };
	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sswndevice);
		
		Log.d(TAG, "onCreate()");
		
		Intent intent = getIntent();
		
		deviceId = intent.getStringExtra("deviceId");

		Log.d(TAG, "onCreate() - deviceId:" + deviceId);
		
		((TextView)findViewById(R.id.sswnDeviceId)).setText(deviceId);
		
		Button button = (Button)findViewById(R.id.sswnGetSpeed);
		button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	device.getSpeed(SSWNDeviceActivity.this);
            }
        });
		button.setEnabled(false);
		
		final Switch sw = (Switch)findViewById(R.id.sswnNotifications);
		sw.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		   @Override
		   public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		 
			   if (sw.isChecked()) {
		    		Toast.makeText(getApplicationContext(), "Notifications enabled", Toast.LENGTH_SHORT).show();
		    		device.enableNotifications(SSWNDeviceActivity.this);
		    	} else {
		    		Toast.makeText(getApplicationContext(), "Notifications disabled", Toast.LENGTH_SHORT).show();
		    		device.disableNotifications(SSWNDeviceActivity.this);
		    	}
		 
		   }
		});
		sw.setEnabled(false);
		
		Button button2 = (Button)findViewById(R.id.sswnSetThreshold);
		button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	showDialog();
            }
        });
		button2.setEnabled(false);
	}
	
	private void showDialog()
    {
         final Dialog d = new Dialog(SSWNDeviceActivity.this);
         d.setTitle("Choose threshold");
         d.setContentView(R.layout.dialog);
         Button b4 = (Button) d.findViewById(R.id.button4);
         Button b3 = (Button) d.findViewById(R.id.button3);
         final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
         np.setMaxValue(10); // max value 10
         np.setMinValue(1);   // min value 1
         np.setWrapSelectorWheel(false);
         np.setValue(currentThreshold);
         //np.setOnValueChangedListener(this);
         b4.setOnClickListener(new OnClickListener()
         {
          @Override
          public void onClick(View v) {
              //tv.setText(String.valueOf(np.getValue())); //set the value to textview
        	  Toast.makeText(getApplicationContext(), "Value: " + np.getValue(), Toast.LENGTH_SHORT).show();
        	  currentThreshold = np.getValue();
        	  device.setThreshold(currentThreshold);
              d.dismiss();
           }    
          });
         b3.setOnClickListener(new OnClickListener()
         {
          @Override
          public void onClick(View v) {
              d.dismiss(); // dismiss the dialog
           }    
          });
       d.show();
    }
	
	@Override
    protected void onStart() {
    	Log.d(TAG, "onStart()");
    	
        super.onStart();

        // Bind to LocalService
        Intent gattServiceIntent = new Intent(this, DephisitService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }
    
    @Override
    protected void onStop() {
    	Log.d(TAG, "onStop()");
    	
        super.onStop();
        
        device.disableNotifications(this);
        
        // Unbind from the service
        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }
    }

    public void onNotificationsClicked(View v)
    {
    	Switch sw = (Switch)v;
    	
    	if (sw.isChecked()) {
    		Toast.makeText(getApplicationContext(), "Notifications enabled", Toast.LENGTH_SHORT).show();
    		device.enableNotifications(this);
    	} else {
    		Toast.makeText(getApplicationContext(), "Notifications disabled", Toast.LENGTH_SHORT).show();
    		device.disableNotifications(this);
    	}
    }

	@Override
	public void onSpeedValue(SSWNDeviceI speedswnDevice, final int speed) {
		runOnUiThread(new Runnable() {
			public void run() {
				((TextView)findViewById(R.id.sswnSpeedValue)).setText(String.valueOf(speed)+"km/h");
			}
		});
	}

	@Override
	public void onSpeedChange(SSWNDeviceI speedswnDevice, final int speed) {
		runOnUiThread(new Runnable() {
			public void run() {
				((TextView)findViewById(R.id.sswnSpeedValue)).setText(String.valueOf(speed)+"km/h");
			}
		});
	}

	@Override
	public void onDephisitDevice(final String deviceId, final boolean available) {
		//if (device.getDeviceId().equals(deviceId)) {
			runOnUiThread(new Runnable() {
				public void run() {
					if (available) {
						device = (SSWNDeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
						
						((Button)findViewById(R.id.sswnGetSpeed)).setEnabled(true);
						((Button)findViewById(R.id.sswnSetThreshold)).setEnabled(true);
						((Switch)findViewById(R.id.sswnNotifications)).setEnabled(true);
						Toast.makeText(getApplicationContext(), "DEPHISIT device available", Toast.LENGTH_SHORT).show();
					} else {
						//device = null;
						
						((Button)findViewById(R.id.sswnGetSpeed)).setEnabled(false);
						((Button)findViewById(R.id.sswnSetThreshold)).setEnabled(false);
						((Switch)findViewById(R.id.sswnNotifications)).setEnabled(false);
						Toast.makeText(getApplicationContext(), "DEPHISIT device unavailable", Toast.LENGTH_SHORT).show();
					}
				}
			});
		//}
	}

	@Override
	@Deprecated
	public void onScanFinished() {
		// TODO Auto-generated method stub
		
	}
}
