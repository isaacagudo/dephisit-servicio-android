/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * SADeviceI - Interface to be implemented by SoundActuator devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface SADeviceI extends DephisitDeviceI {

	boolean playSound();
}
