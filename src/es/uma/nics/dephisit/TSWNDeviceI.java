/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * TSWNDeviceI - Interface to be implemented by TemperatureSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface TSWNDeviceI extends DephisitDeviceI {

	boolean getTemp(TSWNDeviceListener tswnDListener);
	
	boolean areNotificationsEnabled();
	boolean enableNotifications(TSWNDeviceListener tswnDListener);
	boolean disableNotifications(TSWNDeviceListener tswnDListener);
	
	float getThreshold();
	boolean setThreshold(float threshold);
}
