/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * SADeviceActivity - Activity for use SoundActuator devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class SADeviceActivity extends Activity implements DADeviceListener, DephisitCallback {

	private final static String TAG = SADeviceActivity.class.getSimpleName();
	
	private DephisitService mBluetoothLeService;
	
	private String deviceId;
	private SADeviceI device;
	
	private boolean mBound;
	
	// Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
        	mBluetoothLeService = ((DephisitService.LocalBinder) service).getService();
        	mBound = true;
        	
        	Log.i(TAG, "BluetoothLeService (API) connected");
        	
        	/*device = (SADeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
        	
        	runOnUiThread(new Runnable() {
    			public void run() {
    				((Button)findViewById(R.id.saPlaySound)).setEnabled(true);
    			}
    		});*/
        	
        	mBluetoothLeService.scanDephisitDevices(SADeviceActivity.this, true, deviceId);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        	mBluetoothLeService = null;
        	mBound = false;
        	
        	Log.i(TAG, "BluetoothLeService (API) disconnected");
        }
    };
	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sadevice);
		
		Log.d(TAG, "onCreate()");
		
		Intent intent = getIntent();
		
		deviceId = intent.getStringExtra("deviceId");

		Log.d(TAG, "onCreate() - deviceId:" + deviceId);
		
		((TextView)findViewById(R.id.saDeviceId)).setText(deviceId);
		
		Button button2 = (Button)findViewById(R.id.saPlaySound);
		button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	device.playSound();
            }
        });
		button2.setEnabled(false);
	}
	
	@Override
    protected void onStart() {
    	Log.d(TAG, "onStart()");
    	
        super.onStart();

        // Bind to LocalService
        Intent gattServiceIntent = new Intent(this, DephisitService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }
    
    @Override
    protected void onStop() {
    	Log.d(TAG, "onStop()");
    	
        super.onStop();
        
        // Unbind from the service
        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }
    }

	@Override
	public void onDephisitDevice(final String deviceId, final boolean available) {
		//if (device.getDeviceId().equals(deviceId)) {
    		runOnUiThread(new Runnable() {
				public void run() {
		    		if (available) {
		    			device = (SADeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
		    			
						((Button)findViewById(R.id.saPlaySound)).setEnabled(true);
						Toast.makeText(getApplicationContext(), "DEPHISIT device available", Toast.LENGTH_SHORT).show();
					} else {
						//device = null;
						
						((Button)findViewById(R.id.saPlaySound)).setEnabled(false);
						Toast.makeText(getApplicationContext(), "DEPHISIT device unavailable", Toast.LENGTH_SHORT).show();
					}
				}
    		});
    	//}
	}

	@Override
	@Deprecated
	public void onScanFinished() {
		// TODO Auto-generated method stub
		
	}
}
