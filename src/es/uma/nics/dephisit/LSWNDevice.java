/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

/**
 * LSWNDevice - Implementation of the LightSensorWithNotifications device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class LSWNDevice extends DephisitDevice implements LSWNDeviceI {
	
	private final static String TAG = LSWNDevice.class.getSimpleName();
	
	private BluetoothGattCharacteristic lightValue = null;
	private BluetoothGattCharacteristic lightThreshold = null;
	
	private LSWNDeviceListener lswnDListener = null;
	
	//TODO Use synchronization?
	//private Boolean free = true;
	//private final Object lock = new Object();
	
	private static enum LSWNChar {LSWN_LIGHT_VALUE, LSWN_LIGHT_THRESHOLD}

	//Read and Notification attribute
	static private final AttMap[] LSWN_LIGHT_VALUE_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000015-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002221-0000-1000-8000-00805f9b34fb")
	};
	
	//Write attribute
	static private final AttMap[] LSWN_LIGHT_THRESHOLD_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000016-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002222-0000-1000-8000-00805f9b34fb")
	};
	
	private final DephisitDeviceCallback callback = new DephisitDeviceCallback() {

		@Override
		public void onValue(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValue() - characteristic: " + characteristic.getUuid());
			
			if (characteristic==lightValue) {
				
				//synchronized(lock) {

				lswnDListener.onLightValue(LSWNDevice.this, DephisitService.getIntLEValueFromCharacteristic(characteristic));

				//free = true;
				//free.notify();
				//}
			}
		}
		
		@Override
		public void onValueChange(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValueChange() - characteristic: " + characteristic.getUuid());
			
			if (characteristic==lightValue) {
				
				final byte[] data = characteristic.getValue();
				Log.i(TAG, "value: " + DephisitUtils.toHexString(data));

				lswnDListener.onLightChange(LSWNDevice.this, DephisitService.getIntLEValueFromCharacteristic(characteristic));
			}
		}
	};
	
	public LSWNDevice(String n, Location l, BluetoothGatt bleDevice, DephisitService bluetoothLeService) {
		super(DeviceType.LSWN, n, l, bleDevice, bluetoothLeService);
	}

	@Override
	public boolean getLight(LSWNDeviceListener lswnDListener) {
		Log.d(TAG, "getLight() - lswnDListener: " + lswnDListener);
		
		/*synchronized(lock) {
			if (!free) {
				try {
					free.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			this.lswnDListener = lswnDListener;
			free = false;
			
			return bluetoothLeService.readCharacteristic(bleDevice, lightValue, callback);
		}*/
		this.lswnDListener = lswnDListener;
		
		return DephisitService.readCharacteristic(bleDevice, lightValue, callback);
	}
	
	//TODO Change for use multiple clients???
	@Override
	public boolean areNotificationsEnabled() {
		return DephisitService.areNotificationsEnabled(lightValue);
	}
	
	@Override
	public boolean enableNotifications(LSWNDeviceListener lswnDListener) {
		this.lswnDListener = lswnDListener;
		return DephisitService.setCharacteristicNotification(bleDevice, lightValue, true, callback);
	}
	
	@Override
	public boolean disableNotifications(LSWNDeviceListener lswnDListener) {
		this.lswnDListener = null;
		return DephisitService.setCharacteristicNotification(bleDevice, lightValue, false, null);
	}
	
	@Override
	public int getThreshold() {
		Log.d(TAG, "getThreshold()");
		
		return DephisitService.getIntLEValueFromCharacteristic(lightThreshold);
	}
	
	@Override
	public boolean setThreshold(int threshold) {
		Log.d(TAG, "setThreshold() - threshold: " + threshold);
		
		if (threshold > 0 && threshold <= 10) {
			DephisitService.setIntLEValueToCharacteristic(lightThreshold, threshold);
			return DephisitService.writeCharacteristic(bleDevice, lightThreshold);
		} else {
			Log.w(TAG, "Ignoring threshold value (value: " + threshold + ")");
			return false;
		}
		
	}

	static String isValidCharacteristic(BluetoothGattCharacteristic characteristic) {
		for (AttMap map : LSWN_LIGHT_VALUE_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) > 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
				return LSWNChar.LSWN_LIGHT_VALUE.toString();
			}
		}
		
		for (AttMap map : LSWN_LIGHT_THRESHOLD_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {
				return LSWNChar.LSWN_LIGHT_THRESHOLD.toString();
			}
		}
		
		return null;
	}

	@Override
	public boolean isComplete() {
		return lightValue!=null && lightThreshold!=null;
	}

	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		switch (LSWNChar.valueOf(charName)) {
		case LSWN_LIGHT_VALUE:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'LightValue' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.lightValue = characteristic;
			break;
		case LSWN_LIGHT_THRESHOLD:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'LightThreshold' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.lightThreshold = characteristic;
			break;
		default:
			break;
		}
	}
}
