/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

//TODO Synchronize calls?
//TODO Create a common interface for this type of devices (detectors)

/**
 * TSDDevice - Implementation of the TrafficSignalsDetector device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class TSDDevice extends DephisitDevice implements TSDDeviceI {
	private final static String TAG = TSDDevice.class.getSimpleName();
	
	private TSDDeviceListener trafficSignalsDetectorDListener = null;
	
	private volatile Map<String, TSBeacon> tsBeaconsTime = new HashMap<String, TSBeacon>();
	
	//private final Object lock = new Object();
	
	class TSBeacon {
		protected float latitude;
		protected float longitude;
		protected int fromDirection;
		protected int toDirection;
		protected byte trafficSign;
		protected long lastTime;
		
		protected TSBeacon(float latitude, float longitude, int fromDirection, int toDirection, byte trafficSign, long lastTime) {
			this.latitude = latitude;
			this.longitude = longitude;
			this.fromDirection = fromDirection;
			this.toDirection = toDirection;
			this.trafficSign = trafficSign;
			this.lastTime = lastTime;
		}
	}
	
	Thread tsdThread = new Thread(new Runnable() {
        public void run() {
        	
        	List<String> keysToDelete = new ArrayList<String>();
        	
        	while (true) {
        		//synchronized (lock) {
        		for (Entry<String, TSBeacon> entryBeacon: tsBeaconsTime.entrySet()) {
        			TSBeacon tsBeacon = entryBeacon.getValue();
        			String tsBeaconId = entryBeacon.getKey();
        			if (System.currentTimeMillis() - 2000 > tsBeacon.lastTime) {
        				if (trafficSignalsDetectorDListener != null) {
        					trafficSignalsDetectorDListener.onTrafficSignOutOfRange(TrafficSign.fromCode(String.valueOf(tsBeacon.trafficSign)),
        							tsBeacon.latitude, tsBeacon.longitude, tsBeacon.fromDirection, tsBeacon.toDirection);
        				}
        				
        				//Delete traffic sign beacon
        				keysToDelete.add(tsBeaconId);
        			}
        		}
        		
        		for (String key: keysToDelete) {
        			tsBeaconsTime.remove(key);
        		}
        		keysToDelete.clear();
        		//}
        		
        		try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
            }
        }
    });
	

	public TSDDevice(String n, Location l) {
		super(DeviceType.TSD, n, l, null, null);
		
		tsdThread.start();
	}
	
	//TODO Change for use multiple clients???
	@Override
	public boolean areNotificationsEnabled() {
		return this.trafficSignalsDetectorDListener!=null;
	}
	
	@Override
	public boolean enableNotifications(TSDDeviceListener trafficSignalsDetectorDListener) {
		this.trafficSignalsDetectorDListener = trafficSignalsDetectorDListener;
		
		//synchronized (lock) {
		for (TSBeacon tsBeacon: tsBeaconsTime.values()) {
			trafficSignalsDetectorDListener.onNewTrafficSignInRange(TrafficSign.fromCode(String.valueOf(tsBeacon.trafficSign)),
					tsBeacon.latitude, tsBeacon.longitude, tsBeacon.fromDirection, tsBeacon.toDirection);
		//}
		}
		
		return true;
	}
	
	@Override
	public boolean disableNotifications(TSDDeviceListener trafficSignalsDetectorDListener) {
		this.trafficSignalsDetectorDListener = null;
		
		return true;
	}
	
	@Override
	public boolean isComplete() {
		return true;
	}

	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		//Do nothing, this is a detector device
	}
	
	void notifyAdvertisement(String id, float latitude, float longitude, byte tsState, short fromDirection, short toDirection) {
		Log.d(TAG, "Traffic Signal Advertisement - id: " + id + ", latitude: " + latitude + ", longitude: " + longitude 
				+ "tsState: " + tsState + ", fromDirection: " + fromDirection + ", toDirection: " + toDirection);
		
		TSBeacon tsBeacon = new TSBeacon(latitude, longitude, fromDirection, toDirection, tsState, System.currentTimeMillis());
	
		//synchronized (lock) {
		if (tsBeaconsTime.containsKey(id) && tsBeaconsTime.get(id).trafficSign == tsState) {
			tsBeaconsTime.put(id, tsBeacon);
			
		} else {
			//Put state in the map
			tsBeaconsTime.put(id, tsBeacon);

			//Notify state to listener
			if (trafficSignalsDetectorDListener != null) {
				trafficSignalsDetectorDListener.onNewTrafficSignInRange(TrafficSign.fromCode(String.valueOf(tsState)),
						tsBeacon.latitude, tsBeacon.longitude, tsBeacon.fromDirection, tsBeacon.toDirection);
			}
		}
		//}
	}
}
