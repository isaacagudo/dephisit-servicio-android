/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import java.lang.reflect.InvocationTargetException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.Map.Entry;

/**
 * DephisitService - Main DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class DephisitService extends Service implements DephisitInterface {
    private final static String TAG = DephisitService.class.getSimpleName();
    
    private Map<String, DephisitDevice> dephisitDevices = new HashMap<String, DephisitDevice>();
    
    //JESUS - Para desconectar e ignorar dispositivos BLE no usados por ningun dispositivo DEPHISIT --
    private List<BluetoothDevice> devicesToIgnore = new ArrayList<BluetoothDevice>();
    //JESUS ------------------------------------------------------------------------------------------
    
    //TODO Use normal Thread class instead of HandlerThread class??
    private HandlerThread scanThread = new HandlerThread("ScanThread");
    
    //JESUS - Para que funcione en S5mini ------
    private Handler handlerBLE;
    //JESUS ------------------------------------
    
    @Deprecated
    private DephisitCallback apiClient;
    @Deprecated
    private Handler mHandler;
    
    private class ApiClientListener {
    	DephisitCallback listener;
    	String regExpId;
    	
    	public ApiClientListener(DephisitCallback listener, String regExpId) {
    		this.listener = listener;
    		this.regExpId = regExpId;
    	}
    	
    	public int hashCode() {
			return listener.hashCode();
    	}
    	
    	public boolean equals(Object o) {
			return o instanceof ApiClientListener && this.listener==((ApiClientListener)o).listener;
    	}
    }
    private Set<ApiClientListener> apiClientsWithRE = new HashSet<ApiClientListener>();
    
    private BluetoothAdapter mBluetoothAdapter;
    
    private static Map<BluetoothGattCharacteristic, DephisitDeviceCallback> characteristicReadMap = 
    		new HashMap<BluetoothGattCharacteristic, DephisitDeviceCallback>();
    /*private static Map<BluetoothGattCharacteristic, DephisitDeviceCallback> characteristicWriteMap = 
    		new HashMap<BluetoothGattCharacteristic, DephisitDeviceCallback>();*/
    private static Map<BluetoothGattCharacteristic, DephisitDeviceCallback> characteristicNotificationMap = 
    		new HashMap<BluetoothGattCharacteristic, DephisitDeviceCallback>();
    
    // Stops scanning after 10 seconds.
    private static final long SCAN_PERIOD = 20000;

	public static final int DEPHISIT_SCAN_DEVICES = 0;
	public static final int DEPHISIT_ON_NEW_DEVICE = 1;

	private boolean initialized = false;
	
	private boolean scanRunning = false;
	
	private Handler uiHandler = new Handler();
	

	private final IBinder mBinder = new LocalBinder();
	
	 public class LocalBinder extends Binder {
	        DephisitService getService() {
	            return DephisitService.this;
	        }
	 }
	
	 
    // Implements callback methods for GATT events that the app cares about.  For example,
    // connection change and services discovered.
    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        
    	//TODO Change to Set?
    	Map<String, DephisitDevice> dephistiDeviceMap = new HashMap<String, DephisitDevice>();
    	
    	@Override
        public void onConnectionStateChange(final BluetoothGatt gatt, int status, int newState) {
    		Log.d(TAG, "onConnectionStateChange() - gatt:" + gatt + ", status:" + status + ", newState:" + newState); 
    		
            if (newState == BluetoothProfile.STATE_CONNECTED) {
               
            	Log.i(TAG, "Connected to GATT server - " + gatt.getDevice().getName());
                
                showToast("Connected to GATT server - " + gatt.getDevice().getName());
                
                // Attempts to discover services after successful connection.
                Log.i(TAG, "Attempting to start service discovery:" + gatt.discoverServices());
                
                Log.i(TAG, "GATT:" + gatt.getDevice());
                
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                
            	Log.i(TAG, "Disconnected from GATT server - " + gatt.getDevice().getName());
                
                showToast("Disconnected from GATT server - " + gatt.getDevice().getName());
                
                //JESUS - Para manejar la desconexion de dispositivos BLE ---------------------------------
                
                //Send notifications to all API clients that listen for DEPHISIT devices that internally use this 
                //BluetoothGatt instance
                for (Entry<String, DephisitDevice> entryDevice: dephisitDevices.entrySet()) {
        			String deviceId = entryDevice.getKey();
        			DephisitDevice device = entryDevice.getValue();
        			
        			//If this DEPHISIT device use internally this BluetoothGatt instance -> Notify API clients
        			//and delete
        			if (gatt!=null && gatt.equals(device.getBluetoothGatt())) {
        				
        				//TODO Necessary? -> Not used currently
        				//Notify to the DephisitDevice instance
        				device.setInRange(false);
                		
        				//Notify to all API clients that listen for this specified DEPHISIT device 
                		for (ApiClientListener apiClient : apiClientsWithRE) {
                			if (deviceId.matches(apiClient.regExpId)) {
                				apiClient.listener.onDephisitDevice(deviceId, false);
                			}
                		} 
                		
                		//Delete device from intermediate map (for search new gatt and characteristics instances)
                		dephistiDeviceMap.remove(deviceId);
        			}
        		}
        		//JESUS -----------------------------------------------------------------------------------
                
                //VERY IMPORTANT CALL CLOSE() BEFORE DESTROY SERVICE!!!!!
                //-> Not needed if we don't call disconnect() from onUnbind()
                //-> Needed for free resources after a BLE device disconnection
                gatt.close();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        	Log.d(TAG, "onServicesDiscovered() - gatt:" + gatt + ", status:" + status); 
        	
            if (status == BluetoothGatt.GATT_SUCCESS) { 
            	
            	//Create a thread for iterate over services and characteristics
            	//Without this thread, we can't read and process multiple descriptors at once 
            	final BluetoothGatt g = gatt;
            	Log.d(TAG, "onServicesDiscovered() - Creating thread..."); 
            	new Thread(new Runnable() {
                    public void run() {
                    	Log.d(TAG, "onServicesDiscovered() - Thread created"); 
                    	
                    	System.out.println("PROCESSING - " + g.getDevice().getName());
                    	
                    	//Iterate over services and characteristics
                    	for (BluetoothGattService service : g.getServices()) {
                    		for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                    			
                    			Log.i(TAG, "New characteristic - " + characteristic.getUuid() + " (ID: " + characteristic.getInstanceId() + ")");
                    			
                    			/*for (BluetoothGattDescriptor d : characteristic.getDescriptors()) {
                    				Log.i(TAG, "Descriptor: - " + d.getUuid() + " (Characteristic: " + characteristic.getUuid() + ", ID: " + characteristic.getInstanceId() + ")");
                    			}*/
                    			
                    			//Get characteristic TLN:
                    			//- If descriptor with UUID=2901 exists, then the TLN string is its value.
                    			//- If descriptor with UUID=2901 doesn't exist, then the TLN string is the bluetooth device name.
                    			BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString("00002901-0000-1000-8000-00805f9b34fb"));
                    			if (descriptor!=null) {
                    				//Get the TLN string form descriptor value, and process the characteristic 
                    				//in onDescriptorRead() method
                    				Log.i(TAG, "Attempting to read descriptor:" + g.readDescriptor(descriptor));
                    				
                    				//JESUS - Para leer los descriptores correctamente -------------
                    				//Wait 500ms between readDescriptor() calls
                    				//try {Thread.sleep(100);} catch (Exception e) {}
                    				synchronized (descriptor) {
                    					try {descriptor.wait();} catch (InterruptedException e) {e.printStackTrace();}
                    				}
                    				//JESUS --------------------------------------------------------
                    			} else {
                    				//Get TLN string from bluetooth device name
                    				String tlnString = g.getDevice().getName();
                    				
                    				Log.v(TAG, "Calling processCharacteristic() from onServicesDiscovered()");
                    				
                    				//Process characteristic here
                        			processCharacteristic(tlnString, g, characteristic);
                    			}
                    		}
                    	}
                    	
                    	System.out.println("PROCESSED - " + g.getDevice().getName());
                    	
                    	//JESUS - Para desconectar e ignorar dispositivos BLE no usados por ningun dispositivo DEPHISIT --
                    	//TODO Do this using a list of DephisitGatt objects?
                    	boolean usedGatt = false;
                    	Iterator<DephisitDevice> it = dephisitDevices.values().iterator();
                    	while (it.hasNext()) {
                    		if (it.next().getBluetoothGatt() == g) {
                	    		usedGatt = true;
                    		}
                    	}
                    	if (!usedGatt) {
                    		devicesToIgnore.add(g.getDevice());
                    		//g.disconnect();
                    		g.close();
                    	}
                    	//JESUS ------------------------------------------------------------------------------------------
                    	
                    	//JESUS - Para manejar la desconexion de dispositivos BLE ---------------------------------
        				//Clear the intermediate map when all GATT's characteristics has been processed
        				//dephistiDeviceMap.clear();
                    	//JESUS -----------------------------------------------------------------------------------
                    }
                }).start();

            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
        	Log.d(TAG, "onCharacteristicRead() - gatt:" + gatt + ", characteristic:" + characteristic + ", status:" + status); 
        	
            if (status == BluetoothGatt.GATT_SUCCESS) {
            	
            	//Return the value of characteristic, if this characteristic has been called from a DephisitDevice
            	if (characteristicReadMap.containsKey(characteristic)) {
            		characteristicReadMap.get(characteristic).onValue(characteristic);
            		characteristicReadMap.remove(characteristic);
            	} else {
            		Log.w(TAG, "Error handling the read of a characteristic (possible change of gatt and/or characteristics instances)"); 
            	}
            	
            } else {
            	Log.w(TAG, "Error reading characteristic " + characteristic.getUuid() + ", status " + status);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
        	Log.d(TAG, "onCharacteristicChanged() - gatt:" + gatt + ", characteristic:" + characteristic); 
        		
        	//Return the value of characteristic, if this characteristic has been called from a DephisitDevice
        	if (characteristicNotificationMap.containsKey(characteristic)) {
        		characteristicNotificationMap.get(characteristic).onValueChange(characteristic);
        	} else {
        		Log.w(TAG, "Error handling the notification of a characteristic (possible change of gatt and/or characteristics instances)"); 
        	}
        }
        
        @Override
        public void  onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
        	Log.d(TAG, "onDescriptorRead() - gatt:" + gatt + ", descriptor:" + descriptor + ", status:" + status); 
        	
        	//JESUS - Para leer los descriptores correctamente -------------
        	synchronized (descriptor) {
        		descriptor.notify();
        	}
        	//JESUS --------------------------------------------------------
        	
        	if (status == BluetoothGatt.GATT_SUCCESS) {
        		
        		String tlnString = new String(descriptor.getValue());
        		
        		Log.v(TAG, "Calling processCharacteristic() from onDescriptorRead()");
        		//Process characteristic
        		processCharacteristic(tlnString, gatt, descriptor.getCharacteristic());
        	} else {
        		Log.w(TAG, "Error reading descriptor, status " + status);
        	}
        }
        
        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
        	Log.d(TAG, "onCharacteristicWrite() - gatt:" + gatt + ", characteristic:" + characteristic + ", status:" + status); 
        }
        
        private void processCharacteristic(String tlnString, BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
        	Log.d(TAG, "processCharacteristic() - tlnString:" + tlnString + ", characteristic:" + characteristic.getUuid());
			
			if (tlnString!=null && tlnString.matches("^.*:.*:.*$")) {
				
				Log.i(TAG, "Valid TLN string found (" + tlnString + ") for this characteristic (" + characteristic.getUuid() + ")");
				
				//All characteristics must be in the same GATT (BLE device)
				//if (dephistiDeviceMap.get(tlnString)==null || !dephistiDeviceMap.get(tlnString).isComplete()) {
				if (dephistiDeviceMap.get(tlnString)==null 
						|| (dephistiDeviceMap.get(tlnString).getBluetoothGatt() == gatt && !dephistiDeviceMap.get(tlnString).isComplete())) {
					
					//Obtain device type, location and name
					String[] tln = tlnString.split(":");
					
					try {
						DeviceType deviceType = DeviceType.valueOf(tln[0]);

						Location dephisitDeviceLocation = Location.UNKNOWN;
						for (Location l : Location.values()) {
							if (l.getCode() == Integer.valueOf(tln[1]).intValue()) {
								dephisitDeviceLocation = l;
							}
						}
						
		    			String dephisitDeviceName = tln[2];
		    			
		    			//Add characteristic to an existing or new Dephisit device
		    			//TIPOtry {
						Class<? extends DephisitDevice> deviceClass = Class.forName(getPackageName()+"."+deviceType.name()+"Device").asSubclass(DephisitDevice.class);					
						
						String deviceChar= (String) deviceClass.getDeclaredMethod("isValidCharacteristic", BluetoothGattCharacteristic.class).invoke(null, characteristic);
	    				
	    				if (deviceChar!=null) {
	    					
	    					Log.i(TAG, "Characteristic (" + characteristic.getUuid() + ") is valid");
	    					
	    					if (!dephistiDeviceMap.containsKey(tlnString)) {
	    						//dephistiDeviceMap.put(tlnString, deviceClass.getConstructor(String.class, Location.class, BluetoothGatt.class, DephisitCallback.class,DephisitService.class).newInstance(dephisitDeviceName, dephisitDeviceLocation, gatt, apiClient, DephisitService.this));
	    						dephistiDeviceMap.put(tlnString, deviceClass.getConstructor(String.class, Location.class, BluetoothGatt.class, DephisitService.class).newInstance(dephisitDeviceName, dephisitDeviceLocation, gatt, DephisitService.this));
	    					}
	    					
	    					deviceClass.cast(dephistiDeviceMap.get(tlnString)).setCharacteristic(deviceChar, characteristic);
	    				} else {
	    					Log.w(TAG, "Invalid characteristic (" + characteristic.getUuid() + ") for dephisit device type " + deviceType.name() + " (" + deviceType + ")");
	    				}
	    				
					/*TIPO} catch (ClassNotFoundException e) {
						e.printStackTrace();
						Log.w(TAG, "Invalid device type (" + deviceType + ") for this characteristic (" + characteristic.getUuid() + "), ignoring it!");
					} catch (NoSuchMethodException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					} catch (InstantiationException e) {
						e.printStackTrace();
					}*/
					} catch (Exception e) {
						e.printStackTrace();
						Log.w(TAG, "Invalid device type (" + tln[0] + ") for this characteristic (" + characteristic.getUuid() + "), ignoring it!");
					}
	    			
	    			//Add the complete dephisit device to list, and notify to API client
	    			DephisitDevice d = dephistiDeviceMap.get(tlnString);
        			if (d!=null && d.isComplete()) {
        				
        				//JESUS - Para manejar la desconexion de dispositivos BLE ---------------------------------
        				//dephisitDevices.put(d.getDeviceId(), d);
        				if (dephisitDevices.get(d.getDeviceId()) == null 
        						|| !dephisitDevices.get(d.getDeviceId()).getInRange()) {
        					dephisitDevices.put(d.getDeviceId(), d);
        				}
        				
        				//TODO The best solution for this is only replace gatt and characteristics objects 
        				//not the entire DephisitDevice object (but this require more work)
        				//JESUS -----------------------------------------------------------------------------------
        				
        				// JESUS - Para iniciar el escaneo BLE con la inicialización del servicio DEPHISIT -------------------------
        				//apiClient.onDephisitDevice(d.getDeviceId(), d.getInRange());
        				
        				for (ApiClientListener apiClient : apiClientsWithRE) {
                			if (d.getDeviceId().matches(apiClient.regExpId)) {
                				apiClient.listener.onDephisitDevice(d.getDeviceId(), d.getInRange());
                			}
                		}
        				// JESUS ---------------------------------------------------------------------------------------------------
        			}
	    			
				} else {
					Log.w(TAG, "The dephisit device identify by this TLN string (" + tlnString + ") is complete, " +
							"ignoring this characteristic (" + characteristic.getUuid() + ")!");
				}
   			
			} else {
				Log.w(TAG, "Unknown TLN string (" + tlnString + ") for this characteristic (" + characteristic.getUuid() + "), ignoring it!");
			}
        }
    };


    @Override
    public IBinder onBind(Intent intent) {
    	
    	Log.d(TAG, "onBind() - intent:" + intent);
    	
    	mBluetoothAdapter = ((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter();
        if (!mBluetoothAdapter.isEnabled()) {
        	Log.w(TAG, "Bluetooth not enabled");
        	//throw new DephisitException("Bluetooth not enabled, enable it before any API call");
        	return null;
        }
        
        // JESUS - Para iniciar el escaneo BLE con la inicialización del servicio DEPHISIT -------------------------        
        //Code for smartphones that support BLE continuous scanning (only a scan is needed for 
        //get new BLE advertisements)
        /*Log.v(TAG, "Calling startLeScan()...");  
        initialized = mBluetoothAdapter.startLeScan(mLeScanCallback);
        if (!initialized) {
        	Log.w(TAG, "Bluetooth LE Scan failed!");  
        }*/
        
        //Code for smartphones that don't support BLE continuous scanning (stop and start 
        //scanning every 500ms to get new BLE advertisements)
        if (!initialized) {
        	//HandlerThread hThread = new HandlerThread("HandlerThread");
        	scanThread.start();
        	final Handler handler = new Handler(scanThread.getLooper());
        	
            //JESUS - Para que funcione en S5mini ------
        	handlerBLE = new Handler(DephisitService.this.getMainLooper());
        	//JESUS ------------------------------------
        	
        	Runnable scan = new Runnable() {
        		@Override
                public void run() {
        			boolean shutdownScanThread = false;
        			while(!shutdownScanThread) {
        				Log.d(TAG, "Calling startLeScan() ...");
            			if(!mBluetoothAdapter.startLeScan(mLeScanCallback)) {
            				Log.w(TAG, "Bluetooth LE Scan failed!"); 
            			}
            			try { Thread.sleep(500); } catch (InterruptedException e) { shutdownScanThread=true; Log.d(TAG, "Shutting down scanning thread ..."); }
            			Log.d(TAG, "Calling stopLeScan() ...");
            			mBluetoothAdapter.stopLeScan(mLeScanCallback);
        			}
        		}
        	};
        	
        	handler.post(scan);
        	
        	//Create and add simulated devices
            //addSimulatedDevices();
          
            //Create and add detector devices
            addDetectorDevices();
            
            initialized = true;
    	}
        //JESUS ----------------------------------------------------------------------------------------------------
        
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
    	
    	Log.d(TAG, "onUnbind() - intent:" + intent);
    	
    	//JESUS - Para terminar correctamente la hebra que realiza el scaneo ------------------------------------
    	//Code for smartphones that support BLE continuous scanning
    	//mBluetoothAdapter.stopLeScan(mLeScanCallback);
    	
    	//Code for smartphones that don't support BLE continuous scanning
    	scanThread.interrupt(); 
    	//JESUS -------------------------------------------------------------------------------------------------
    	
    	Iterator<DephisitDevice> it = dephisitDevices.values().iterator();
    	while (it.hasNext()) {
    		BluetoothGatt gatt = it.next().getBluetoothGatt();
    		if (gatt != null) {
	    		//gatt.disconnect(); -> Not needed
	    		gatt.close(); //VERY IMPORTANT!!!!!
    		}
    	}
    	
        return super.onUnbind(intent);
    }


    @Override
    @Deprecated
    public boolean initialize(DephisitCallback listener) throws DephisitException {
    	Log.d(TAG, "initialize() - listener:" + listener);

    	//FOR BACKWARD COMPATIBILITY -------------------------------------------------------------------------------
        apiClient = listener;
        
        return true;
        //----------------------------------------------------------------------------------------------------------
    }
    
    private byte[] getDephisitBeaconData(byte[] scanRecord) {
    	int i = 0;
    	do {
    		byte eirLen = scanRecord[i];

    		if (eirLen != 0) {
	    		byte eirType = scanRecord[i+1];
	    		
	    		if (eirType == DephisitBeacon.mdEIRType) {
	    			if (eirLen > 3 && scanRecord[i+2] == DephisitBeacon.dephisitBeaconId[1] 
	    					&& scanRecord[i+3] == DephisitBeacon.dephisitBeaconId[0]) {
	    				return Arrays.copyOfRange(scanRecord, i+4, i+1+eirLen);
	    			} else {
	    				return null;
	    			}
	    		} else {
	    			i = i+1 + eirLen;
	    		}
    		} else {
    			return null;
    		}    		
    	} while (i < scanRecord.length);

    	return null;
    }
    
    // Device scan callback.
    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        
        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
        	Log.i(TAG, "New BLE device found - " + device.getAddress() + " (" + device.getName() + ")");
        	
        	//showToast("New BLE device found - " + device.getAddress() + " (" + device.getName() + ")");
        	//showNotification(device.getAddress() + " (" + device.getName() + ")"); -> This cause lags in UI thread
        	
        	Log.v(TAG, "scanRecord: " + DephisitUtils.toHexString(scanRecord));
        	Log.v(TAG, "Length of scanRecord: " + scanRecord.length);
        	Log.v(TAG, "Device name: " + device.getName());
        	
        	byte[] dephisitBeaconData = getDephisitBeaconData(scanRecord);
        	
        	if (dephisitBeaconData != null) {
        		Log.i(TAG, "Processing BLE device (" + device.getName() + ") as DEPHISIT beacon");
        		
        		Log.v(TAG, "dephisitBeaconData: " + DephisitUtils.toHexString(dephisitBeaconData));
        		
        		ByteBuffer dephisitAdvertisement = ByteBuffer.wrap(dephisitBeaconData).order(ByteOrder.LITTLE_ENDIAN);
            	
            	float latitude = dephisitAdvertisement.getFloat();
        		float longitude = dephisitAdvertisement.getFloat();
        		byte beaconType = dephisitAdvertisement.get();
        		
        		Log.v(TAG, "Latitude: " + latitude);
        		Log.v(TAG, "Longitude: " + longitude);
        		Log.v(TAG, "beaconType: " + beaconType);
        		
        		switch (DephisitBeacon.DephisitBeaconType.fromCode(String.valueOf(beaconType))) {
        			case TSBEACON:
    	    			byte tsState = dephisitAdvertisement.get();
    	        		short fromDirection = dephisitAdvertisement.getShort();
    	        		short toDirection = dephisitAdvertisement.getShort();
    	        		
    	        		Log.v(TAG, "tsState: " + tsState);
    	        		Log.v(TAG, "fromDirection: " + fromDirection);
    	        		Log.v(TAG, "toDirection: " + toDirection);
    	        		
    	        		for (DephisitDevice d : dephisitDevices.values()) {
            				if (d instanceof TSDDevice) {
            					//((TSDDevice)d).notifyAdvertisement(latitude, longitude, tsState, fromDirection, toDirection);
            					((TSDDevice)d).notifyAdvertisement(device.getAddress(), latitude, longitude, tsState, fromDirection, toDirection);
            				}
            			}
            			break;
        			case WBEACON:
        				byte roadState = dephisitAdvertisement.get();
    	        		
    	        		Log.v(TAG, "roadState: " + roadState);
    	        		
    	        		for (DephisitDevice d : dephisitDevices.values()) {
            				if (d instanceof WDDevice) {
            					//((WDDevice)d).notifyAdvertisement(latitude, longitude, roadState);
            					((WDDevice)d).notifyAdvertisement(device.getAddress(), latitude, longitude, roadState);
            				}
            			}
            			break;
        			case BBEACON:
        				byte bicycleState = dephisitAdvertisement.get();
    	        		
    	        		Log.v(TAG, "bicycleState: " + bicycleState);
    	        		
    	        		for (DephisitDevice d : dephisitDevices.values()) {
            				if (d instanceof BDDevice) {
            					//((BDDevice)d).notifyAdvertisement(latitude, longitude, bicycleState);
            					((BDDevice)d).notifyAdvertisement(device.getAddress(), latitude, longitude, bicycleState);
            				}
            			}
            			break;
        			default:
        				Log.w(TAG, "Unknown DEPHISIT beacon type (" + beaconType + "), ignoring advertisement!");
            			break;
        		}
        	} else {
        		
        		//JESUS - Para desconectar e ignorar dispositivos BLE no usados por ningun dispositivo DEPHISIT --
        		//Log.i(TAG, "Processing BLE device (" + device.getName() + ") as DEPHISIT device");
        		//BluetoothGatt deviceGatt = device.connectGatt(DephisitService.this, false, mGattCallback);
        		
        		if (!devicesToIgnore.contains(device)) {
        			Log.i(TAG, "Processing BLE device (" + device.getName() + ") as DEPHISIT device");
        		    //JESUS - Para que funcione en S5mini ------
        			///*BluetoothGatt deviceGatt = */device.connectGatt(DephisitService.this, false, mGattCallback);
        			
                	handlerBLE.post(new Runnable() {
                		@Override
                        public void run() {
                			/*BluetoothGatt deviceGatt = */device.connectGatt(DephisitService.this, false, mGattCallback);
                		}});
                	//JESUS ------------------------------------
        		} else {
        			Log.i(TAG, "Ignoring BLE device (" + device.getName() + "), previously processed as DEPHISIT device");
        		}
        		//JESUS ------------------------------------------------------------------------------------------
        	}
        }
    };

    
    @Override
    @Deprecated
    public boolean scanDephisitDevices(final boolean enable) throws DephisitException {
    	Log.d(TAG, "scanDephisitDevices() - enable:" + enable);  
  
    	//FOR BACKWARD COMPATIBILITY -------------------------------------------------------------------------------
    	if (enable) {   		

    		if (apiClient != null) scanDephisitDevices(apiClient, true);
	
    		//TODO Keep onScanFinished() method??
    		mHandler = new Handler();
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(scanRunning) {
                    	scanDephisitDevices(apiClient, false); apiClient = null;

                    	apiClient.onScanFinished();
                    }
                }
            }, SCAN_PERIOD);
    	} else {
    		scanDephisitDevices(apiClient, false); apiClient = null;
    	}
    	
    	return true;
    	//----------------------------------------------------------------------------------------------------------
    }

    private void addSimulatedDevices() {

    	//Create simulated devices
    	DephisitDevice[] simulatedDevices = new DephisitDevice[] {
    		new SimulatedPSWNDevice("Simulated GPS", Location.DASHBOARD),
    		new SimulatedSSWNDevice("Simulated Speed Sensor", Location.DASHBOARD),
    		new SimulatedTSDDevice("Simulated Traffic Signals Detector", Location.DASHBOARD)};
    	
    	//Add simulated devices to the map
    	for (DephisitDevice d : simulatedDevices) {
    		dephisitDevices.put(d.getDeviceId(), d);
    	}
    }
    
    //JESUS - Modified code for include TSD device demo --------------------------------------------------------
    private void addDetectorDevices() {

    	//Create detector devices
    	DephisitDevice[] detectorDevices = new DephisitDevice[] {
    		new TSDDevice("Traffic Signals Detector", Location.DASHBOARD),
    		new WDDevice("Weather Detector", Location.DASHBOARD),
    		new BDDevice("Bicycle Detector", Location.DASHBOARD)};
    	
    	//Add detector devices to the map
    	for (DephisitDevice d : detectorDevices) {
    		dephisitDevices.put(d.getDeviceId(), d);
    	}
    }
    //JESUS ----------------------------------------------------------------------------------------------------
    
    @Override
	public DephisitDevice getDephisitDevice(String deviceId) {
		return dephisitDevices.get(deviceId);
	}
    
    
    //TODO Remain static or change to dynamic these methods?
    // Static methods for read/write BLE characteristics -----------------------------------------------------------
    
    // Read a BLE characteristic //
    
	static boolean readCharacteristic(
			BluetoothGatt bleDevice, BluetoothGattCharacteristic characteristic, DephisitDeviceCallback callback) {
		
		characteristicReadMap.put(characteristic, callback);
		
		Log.d(TAG, "BLE <-- (((");
		
		return bleDevice.readCharacteristic(characteristic);
	}

	
	// Enable/disable the notifications of a BLE characteristic //
	
	static boolean setCharacteristicNotification(BluetoothGatt bleDevice,
			BluetoothGattCharacteristic characteristic, boolean enable, DephisitDeviceCallback callback) {

		bleDevice.setCharacteristicNotification(characteristic, enable);
		BluetoothGattDescriptor descriptor = characteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb"));
		
		if (enable) {
			characteristicNotificationMap.put(characteristic, callback);
			descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
		} else {
			characteristicNotificationMap.remove(characteristic);
			descriptor.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
		}
        
        return bleDevice.writeDescriptor(descriptor);
	}

	
	// Get the status of the notifications of a BLE characteristic //
	
	static boolean areNotificationsEnabled(BluetoothGattCharacteristic characteristic) {
		return characteristic.getDescriptor(UUID.fromString("00002902-0000-1000-8000-00805f9b34fb")).getValue().equals(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
	}
	
	
	//TODO Manage runtime exceptions in writeXxxValueToCharacteristic() methods?
    // Write a BLE characteristic //
    
	static boolean writeCharacteristic(BluetoothGatt bleDevice, BluetoothGattCharacteristic characteristic) {
		
		Log.d(TAG, "BLE --> )))");
		
		return bleDevice.writeCharacteristic(characteristic);
	}
	
	
	// Set values to a BLE characteristic //
	
    static boolean setCharLEValueToCharacteristic(BluetoothGattCharacteristic characteristic, char value) {
		
    	boolean res = characteristic.setValue(ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putChar(value).array());
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setCharBEValueToCharacteristic(BluetoothGattCharacteristic characteristic, char value) {
		
    	boolean res = characteristic.setValue(ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN).putChar(value).array());
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setDoubleLEValueToCharacteristic(BluetoothGattCharacteristic characteristic, double value) {
		
    	boolean res = characteristic.setValue(ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putDouble(value).array());
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setDoubleBEValueToCharacteristic(BluetoothGattCharacteristic characteristic, double value) {
		
    	boolean res = characteristic.setValue(ByteBuffer.allocate(8).order(ByteOrder.BIG_ENDIAN).putDouble(value).array());
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setLongLEValueToCharacteristic(BluetoothGattCharacteristic characteristic, long value) {
		
    	boolean res = characteristic.setValue(ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putLong(value).array());
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setLongBEValueToCharacteristic(BluetoothGattCharacteristic characteristic, long value) {
		
    	boolean res = characteristic.setValue(ByteBuffer.allocate(8).order(ByteOrder.BIG_ENDIAN).putLong(value).array());
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setShortLEValueToCharacteristic(BluetoothGattCharacteristic characteristic, short value) {
		
    	boolean res = characteristic.setValue(ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(value).array());
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setShortBEValueToCharacteristic(BluetoothGattCharacteristic characteristic, short value) {
		
    	boolean res = characteristic.setValue(ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN).putShort(value).array());
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setFloatLEValueToCharacteristic(BluetoothGattCharacteristic characteristic, float value) {
		
    	boolean res = characteristic.setValue(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putFloat(value).array());
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setFloatBEValueToCharacteristic(BluetoothGattCharacteristic characteristic, float value) {
		
    	boolean res = characteristic.setValue(ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putFloat(value).array());
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setIntLEValueToCharacteristic(BluetoothGattCharacteristic characteristic, int value) {
		
		//boolean res = characteristic.setValue(value, BluetoothGattCharacteristic.FORMAT_SINT32, 0); //This method writes integers in LE over BLE
    	boolean res = characteristic.setValue(ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(value).array());
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setIntBEValueToCharacteristic(BluetoothGattCharacteristic characteristic, int value) {
		
    	boolean res = characteristic.setValue(ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(value).array());
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setByteValueToCharacteristic(BluetoothGattCharacteristic characteristic, byte value) {
		
    	boolean res = characteristic.setValue(new byte[] {value});
		
		Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
		
		return res;
	}
    
    static boolean setStringValueToCharacteristic(BluetoothGattCharacteristic characteristic, String value) {
		
    	//TODO Notify client about values that exceed maximum size (20 bytes)?
    	//boolean res = characteristic.setValue(value); //This method writes strings over BLE
    	boolean res = characteristic.setValue(value.getBytes()); //TODO Use a specified charset in getBytes()
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setBooleanValueToCharacteristic(BluetoothGattCharacteristic characteristic, boolean value) {
		
    	boolean res = characteristic.setValue(new byte[] {value?(byte)0x01:(byte)0x00});
    	
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
    	
		return res;
	}
    
    static boolean setByteArrayValueToCharacteristic(BluetoothGattCharacteristic characteristic, byte[] value) {
    	
    	//TODO Notify client about values that exceed maximum size (20 bytes)?
    	boolean res = characteristic.setValue(value);
		
		Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " >> BLE");
		
		return res;
	}
    
    
    //TODO Manage runtime exceptions in getXxxValueFromCharacteristic() methods?
    //TODO Add characteristic.getXxxValue() values in comments for each method
    // Read values from a BLE characteristic //
    
    static char getCharLEValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getCharLEValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).getChar();
	}
    
    static char getCharLEValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getCharLEValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).getChar(offset);
	}
    
    static char getCharBEValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getCharBEValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.BIG_ENDIAN).getChar();
	}
    
    static char getCharBEValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getCharBEValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.BIG_ENDIAN).getChar(offset);
	}
    
    static double getDoubleLEValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getDoubleLEValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).getDouble();
	}
    
    static double getDoubleLEValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getDoubleLEValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).getDouble(offset);
	}
    
    static double getDoubleBEValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getDoubleBEValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.BIG_ENDIAN).getDouble();
	}
    
    static double getDoubleBEValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getDoubleBEValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.BIG_ENDIAN).getDouble(offset);
	}
    
    static long getLongLEValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getLongLEValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).getLong();
	}
    
    static long getLongLEValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getLongLEValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).getLong(offset);
	}
    
    static long getLongBEValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getLongBEValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.BIG_ENDIAN).getLong();
	}
    
    static long getLongBEValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getLongBEValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.BIG_ENDIAN).getLong(offset);
	}
    
    static short getShortLEValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getShortLEValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).getShort();
	}
    
    static short getShortLEValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getShortLEValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).getShort(offset);
	}
    
    static short getShortBEValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getShortBEValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.BIG_ENDIAN).getShort();
	}
    
    static short getShortBEValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getShortBEValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.BIG_ENDIAN).getShort(offset);
	}
    
    static float getFloatLEValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getFloatLEValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).getFloat();
	}
    
    static float getFloatLEValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getFloatLEValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).getFloat(offset);
	}
    
    static float getFloatBEValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getFloatBEValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.BIG_ENDIAN).getFloat();
	}
    
    static float getFloatBEValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getFloatBEValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.BIG_ENDIAN).getFloat(offset);
	}
    
    static int getIntLEValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getIntLEValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).getInt();
	}
    
    static int getIntLEValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getIntLEValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).getInt(offset);
	}
    
    static int getIntBEValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getIntBEValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.BIG_ENDIAN).getInt();
	}
    
    static int getIntBEValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getIntBEValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.BIG_ENDIAN).getInt(offset);
	}
    
    static byte getByteValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getByteValueFromCharacteristic()");
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).get();
	}
    
    static byte getByteValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getByteValueFromCharacteristic() - offset: " + offset);
    	
    	return ByteBuffer.wrap(characteristic.getValue()).order(ByteOrder.LITTLE_ENDIAN).get(offset);
	}
    
    static String getStringValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getStringValueFromCharacteristic()");
    	
    	return new String(characteristic.getValue()); //TODO Use a specified charset in new String()
	}
    
    static String getStringValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset, int length) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getStringValueFromCharacteristic() - offset: " + offset + ", length: " + length);
    	
    	return new String(characteristic.getValue(), offset, length); //TODO Use a specified charset in new String()
	}
    
    static boolean getBooleanValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getBooleanValueFromCharacteristic()");
    	
    	return characteristic.getValue()[0]==(byte)0x01;
	}
    
    static boolean getBooleanValueFromCharacteristic(BluetoothGattCharacteristic characteristic, int offset) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getBooleanValueFromCharacteristic() - offset: " + offset);
    	
    	return characteristic.getValue()[offset]==(byte)0x01;
	}
    
    static byte[] getByteArrayValueFromCharacteristic(BluetoothGattCharacteristic characteristic) {
    	Log.d(TAG, DephisitUtils.toHexString(characteristic.getValue()) + " << BLE | getByteArrayValueFromCharacteristic()");
    	
    	return characteristic.getValue();
	}

	// -------------------------------------------------------------------------------------------------------------
	
	
	@Override
    public void onDestroy() {
    	Log.d(TAG, "onDestroy()");
    	
        super.onDestroy();
        
    }
	
	@Override
    public void onCreate() {
    	Log.d(TAG, "onCreate()");
    	
    	super.onCreate();
	}
	
	private void showNotification(String deviceId) {    	
		Notification.Builder nBuilder = new Notification.Builder(this)
			.setSmallIcon(R.drawable.ic_launcher)
			.setContentTitle("New BLE device found")
			.setContentText(deviceId);
	
		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mNotificationManager.notify(deviceId.hashCode(), nBuilder.build());
	}
	
	private void showToast(final String message) {    
		uiHandler.post(new Runnable() {
			public void run() {
				Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
			}
		});
	}
	
	
	@Override
	public boolean scanDephisitDevices(DephisitCallback listener, boolean enable) {
		
		if (enable) {

			//Notify current state
			for (DephisitDevice d : dephisitDevices.values()) {
				if (d.getDeviceId().matches(".*")) {
					listener.onDephisitDevice(d.getDeviceId(), d.getInRange());
				}
			}
			
			//For notify future changes of the state
			return apiClientsWithRE.add(new ApiClientListener(listener, ".*"));
			
		} else {
			
			return apiClientsWithRE.remove(new ApiClientListener(listener, null));
		}
	}

	@Override
	public boolean scanDephisitDevices(DephisitCallback listener, boolean enable, String regExpId) {
		
		if (regExpId == null) {
			return false;
			
		} else {

			if (enable) {

				//Notify current state
				for (DephisitDevice d : dephisitDevices.values()) {
					if (d.getDeviceId().matches(regExpId)) {
						listener.onDephisitDevice(d.getDeviceId(), d.getInRange());
					}
				}
				
				//For notify future changes of the state
				return apiClientsWithRE.add(new ApiClientListener(listener, regExpId));
				
			} else {
		
				return apiClientsWithRE.remove(new ApiClientListener(listener, null));
			}
		}
	}
}
