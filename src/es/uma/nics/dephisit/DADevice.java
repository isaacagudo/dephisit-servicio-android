/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

/**
 * DADevice - Implementation of the DisplayActuator device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class DADevice extends DephisitDevice implements DADeviceI {

	private final static String TAG = DADevice.class.getSimpleName();
	
	private BluetoothGattCharacteristic displayValue = null;
	
	//TODO Use synchronization?
	//private Boolean free = true;
	//private final Object lock = new Object();
	
	private static enum DAChar {DA_DISPLAY_VALUE}
	
	//Write attribute
	private static final AttMap[] DA_DISPLAY_VALUE_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000006-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002222-0000-1000-8000-00805f9b34fb")
	};

	
	public DADevice(String n, Location l, BluetoothGatt bleDevice, DephisitService bluetoothLeService) {
		super(DeviceType.DA, n, l, bleDevice, bluetoothLeService);
	}
	
	@Override
	public boolean setMessage(String message) {
		Log.d(TAG, "setMessage() - message: " + message);
		
		DephisitService.setStringValueToCharacteristic(displayValue, message);
		return DephisitService.writeCharacteristic(bleDevice, displayValue);
	}
	
	static String isValidCharacteristic(BluetoothGattCharacteristic characteristic) {
		for (AttMap map : DA_DISPLAY_VALUE_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {
				return DAChar.DA_DISPLAY_VALUE.toString();
			}
		}
		return null;
	}

	@Override
	public boolean isComplete() {
		return this.displayValue!=null;
	}
	
	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		switch (DAChar.valueOf(charName)) {
		case DA_DISPLAY_VALUE:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'DisplayValue' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.displayValue = characteristic;
			break;
		default:
			break;
		}
	}
}
