/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * SSWNDeviceI - Interface to be implemented by SpeedSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface SSWNDeviceI extends DephisitDeviceI {

	boolean getSpeed(SSWNDeviceListener speedswnDListener);

	boolean areNotificationsEnabled();	
	boolean enableNotifications(SSWNDeviceListener speedswnDListener);
	boolean disableNotifications(SSWNDeviceListener speedswnDListener);

	int getThreshold();
	boolean setThreshold(int threshold);
}
