/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * TSDeviceI - Interface to be implemented by TemperatureSensor devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface TSDeviceI extends DephisitDeviceI {

	boolean getTemp(TSDeviceListener tsDListener);
}
