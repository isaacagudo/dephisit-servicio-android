/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * BDDeviceI - Interface to be implemented by BicycleDetector devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface BDDeviceI extends DephisitDeviceI {

	boolean areNotificationsEnabled();
	boolean enableNotifications(BDDeviceListener bicycleDetectorDListener);
	boolean disableNotifications(BDDeviceListener bicycleDetectorDListener);
}
