/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

/**
 * LSDevice - Implementation of the LightSensor device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class LSDevice extends DephisitDevice implements LSDeviceI {

	private final static String TAG = LSDevice.class.getSimpleName();
	
	private BluetoothGattCharacteristic lightValue = null;
	
	private LSDeviceListener lsDListener = null;
	
	//TODO Use synchronization?
	//private Boolean free = true;
	//private final Object lock = new Object();
	
	private static enum LSChar {LS_LIGHT_VALUE}
	
	//Read attribute
	private static final AttMap[] LS_LIGHT_VALUE_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000014-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002221-0000-1000-8000-00805f9b34fb")
	};

	private final DephisitDeviceCallback callback = new DephisitDeviceCallback() {

		@Override
		public void onValue(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValue() - characteristic: " + characteristic.getUuid());
			
			if (characteristic==lightValue) {
				
				//synchronized(lock) {
				
				lsDListener.onLightValue(LSDevice.this, DephisitService.getIntLEValueFromCharacteristic(characteristic));
				
				//free = true;
				//free.notify();
				//}
			}
		}
	};
	
	
	public LSDevice(String n, Location l, BluetoothGatt bleDevice, DephisitService bluetoothLeService) {
		super(DeviceType.LS, n, l, bleDevice, bluetoothLeService);
	}

	@Override
	public boolean getLight(LSDeviceListener lsDListener) {
		Log.d(TAG, "getLight() - lsDListener: " + lsDListener);
		
		/*synchronized(lock) {
			if (!free) {
				try {
					free.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			this.lsDListener = lsDListener;
			free = false;
			
			return bluetoothLeService.readCharacteristic(bleDevice, lightValue, callback);
		}*/
		this.lsDListener = lsDListener;
		
		return DephisitService.readCharacteristic(bleDevice, lightValue, callback);
	}
	
	static String isValidCharacteristic(BluetoothGattCharacteristic characteristic) {
		for (AttMap map : LS_LIGHT_VALUE_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
				return LSChar.LS_LIGHT_VALUE.toString();
			}
		}
		return null;
	}

	@Override
	public boolean isComplete() {
		return this.lightValue!=null;
	}
	
	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		switch (LSChar.valueOf(charName)) {
		case LS_LIGHT_VALUE:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'LightValue' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.lightValue = characteristic;
			break;
		default:
			break;
		}
	}
}
