/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * DephisitDeviceI - Interface implemented by all DEPHISIT devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface DephisitDeviceI  {

	public String getName();

	public Location getLocation();
	
	public DeviceType getType();
	
	public String getDeviceId();
}
