/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGattCharacteristic;

/**
 * SimulatedDephisitDevice - Abstract class that define common functionality of all simulated DEPHISIT actuator and sensor devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class SimulatedDephisitDevice extends DephisitDevice{
	
	public SimulatedDephisitDevice(DeviceType type, String name, Location location) {
		//Simulated devices doesn't interact with Bluetooth
		super(type, name, location, null, null);
	}

	@Override
	public boolean isComplete() {
		//Simulated devices always are completed after creation
		return true;
	}

	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		//Do nothing, this is a simulated device
	}
}
