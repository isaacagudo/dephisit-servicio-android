/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * LADeviceI - Interface to be implemented by LightActuator devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface LADeviceI extends DephisitDeviceI {

	boolean setLightState(boolean state);
}
