/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * DephisitInterface - Interface implemented by DEPHISIT's service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface DephisitInterface {

	//For use DephisitDevice objects from API client
	@Deprecated
	public boolean initialize(DephisitCallback listener) throws DephisitException;
	@Deprecated
	public boolean scanDephisitDevices(final boolean enable) throws DephisitException;
	public DephisitDevice getDephisitDevice(String deviceId);

	//New methods for scan DPEHIST devices
	public boolean scanDephisitDevices(DephisitCallback listener, boolean enable);
	public boolean scanDephisitDevices(DephisitCallback listener, boolean enable, String regExpId);
	
	//For don't use DephisitDevice objects from API client
	//public List<String> listDevices() throws DephisitException;
	//public boolean getTemp(String deviceId);
	//public boolean getPresure(String deviceId);
}
