/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * PSWNDeviceI - Interface to be implemented by PositionSensorWithNotifications devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface PSWNDeviceI extends DephisitDeviceI {

	boolean getPosition(PSWNDeviceListener pswnDListener);

	boolean areNotificationsEnabled();	
	boolean enableNotifications(PSWNDeviceListener pswnDListener);
	boolean disableNotifications(PSWNDeviceListener pswnDListener);

	float getThreshold();
	boolean setThreshold(float threshold);
}
