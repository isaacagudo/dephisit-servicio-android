/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

/**
 * TSWNDevice - Implementation of the TemperatureSensorWithNotifications device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class TSWNDevice extends DephisitDevice implements TSWNDeviceI {
	private final static String TAG = TSWNDevice.class.getSimpleName();
	
	private BluetoothGattCharacteristic temperatureValue = null;
	private BluetoothGattCharacteristic temperatureThreshold = null;
	
	private TSWNDeviceListener tswnDListener = null;
	
	//TODO Use synchronization?
	//private Boolean free = true;
	//private final Object lock = new Object();
	
	private static enum TSWNChar {TSWN_TEMPERATURE_VALUE, TSWN_TEMPERATURE_THRESHOLD}
	
	//Read and Notification attribute
	static private final AttMap[] TSWN_TEMPERATURE_VALUE_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000012-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002221-0000-1000-8000-00805f9b34fb")
	};
	
	//Write attribute
	static private final AttMap[] TSWN_TEMPERATURE_THRESHOLD_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000013-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002222-0000-1000-8000-00805f9b34fb")
	};
	
	private final DephisitDeviceCallback callback = new DephisitDeviceCallback() {

		@Override
		public void onValue(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValue() - characteristic: " + characteristic.getUuid());
			
			if (characteristic==temperatureValue) {
				
				//synchronized(lock) {
				
				tswnDListener.onTempValue(TSWNDevice.this, DephisitService.getFloatLEValueFromCharacteristic(characteristic));
				
				//free = true;
				//free.notify();
				//}
			}
		}
		
		@Override
		public void onValueChange(BluetoothGattCharacteristic characteristic) {
			Log.d(TAG, "onValueChange() - characteristic: " + characteristic.getUuid());
			
			if (characteristic==temperatureValue) {
				
				tswnDListener.onTempChange(TSWNDevice.this, DephisitService.getFloatLEValueFromCharacteristic(characteristic));
			}
		}
	};
	
	public TSWNDevice(String n, Location l, BluetoothGatt bleDevice, DephisitService bluetoothLeService) {
		super(DeviceType.TSWN, n, l, bleDevice, bluetoothLeService);
	}

	@Override
	public boolean getTemp(TSWNDeviceListener tswnDListener) {
		Log.d(TAG, "getTemp() - tswnDListener: " + tswnDListener);
		
		/*synchronized(lock) {
			if (!free) {
				try {
					free.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			this.tswnDListener = tswnDListener;
			free = false;
			
			return bluetoothLeService.readCharacteristic(bleDevice, temperatureValue, callback);
		}*/
		this.tswnDListener = tswnDListener;
		
		return DephisitService.readCharacteristic(bleDevice, temperatureValue, callback);
	}
	
	//TODO Change for use multiple clients???
	@Override
	public boolean areNotificationsEnabled() {
		return DephisitService.areNotificationsEnabled(temperatureValue);
	}
	
	@Override
	public boolean enableNotifications(TSWNDeviceListener tswnDListener) {
		this.tswnDListener = tswnDListener;
		return DephisitService.setCharacteristicNotification(bleDevice, temperatureValue, true, callback);
	}
	
	@Override
	public boolean disableNotifications(TSWNDeviceListener tswnDListener) {
		this.tswnDListener = null;
		return DephisitService.setCharacteristicNotification(bleDevice, temperatureValue, false, null);
	}
	
	@Override
	public float getThreshold() {
		Log.d(TAG, "getThreshold()");
		
		return DephisitService.getFloatLEValueFromCharacteristic(temperatureThreshold);
	}
	
	@Override
	public boolean setThreshold(float threshold) {
		Log.d(TAG, "setThreshold() - threshold: " + threshold);
		
		if (threshold > 0.0 && threshold <= 10.0) {
			DephisitService.setFloatLEValueToCharacteristic(temperatureThreshold, threshold);
			return DephisitService.writeCharacteristic(bleDevice, temperatureThreshold);
		} else {
			Log.w(TAG, "Ignoring threshold value (value: " + threshold + ")");
			return false;
		}
	}

	static String isValidCharacteristic(BluetoothGattCharacteristic characteristic) {		
		for (AttMap map : TSWN_TEMPERATURE_VALUE_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) > 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
				return TSWNChar.TSWN_TEMPERATURE_VALUE.toString();
			}
		}
		
		for (AttMap map : TSWN_TEMPERATURE_THRESHOLD_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {
				return TSWNChar.TSWN_TEMPERATURE_THRESHOLD.toString();
			}
		}
		
		return null;
	}
	
	@Override
	public boolean isComplete() {
		return temperatureValue!=null && temperatureThreshold!=null;
	}

	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		switch (TSWNChar.valueOf(charName)) {
		case TSWN_TEMPERATURE_VALUE:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'TemperatureValue' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.temperatureValue = characteristic;
			break;
		case TSWN_TEMPERATURE_THRESHOLD:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'TemperatureThreshold' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.temperatureThreshold = characteristic;
			break;
		default:
			break;
		}
	}
}
