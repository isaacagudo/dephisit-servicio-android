/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

/**
 * SADevice - Implementation of the SoundActuator device, used by DEPHISIT service.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class SADevice extends DephisitDevice implements SADeviceI {

	private final static String TAG = SADevice.class.getSimpleName();
	
	private BluetoothGattCharacteristic soundValue = null;
	
	//TODO Use synchronization?
	//private Boolean free = true;
	//private final Object lock = new Object();
	
	private static enum SAChar {SA_SOUND_VALUE}
	
	//Read attribute
	private static final AttMap[] SA_SOUND_VALUE_MAPS = {
		new AttMap("00000000-0000-1000-8000-00805f9b34fb", "00000008-0000-1000-8000-00805f9b34fb"),
		new AttMap("00002220-0000-1000-8000-00805f9b34fb", "00002222-0000-1000-8000-00805f9b34fb")
	};

	
	public SADevice(String n, Location l, BluetoothGatt bleDevice, DephisitService bluetoothLeService) {
		super(DeviceType.SA, n, l, bleDevice, bluetoothLeService);
	}
	
	@Override
	public boolean playSound() {
		DephisitService.setByteValueToCharacteristic(soundValue, (byte)0xFF);
		return DephisitService.writeCharacteristic(bleDevice, soundValue);
	}
	
	static String isValidCharacteristic(BluetoothGattCharacteristic characteristic) {
		for (AttMap map : SA_SOUND_VALUE_MAPS) {
			if (map.serviceUUID.compareTo(characteristic.getService().getUuid()) == 0
					&& map.characteristicUUID.compareTo(characteristic.getUuid()) == 0
					&& (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) > 0) {
				return SAChar.SA_SOUND_VALUE.toString();
			}
		}
		return null;
	}

	@Override
	public boolean isComplete() {
		return this.soundValue!=null;
	}
	
	@Override
	void setCharacteristic(String charName, BluetoothGattCharacteristic characteristic) {
		switch (SAChar.valueOf(charName)) {
		case SA_SOUND_VALUE:
			Log.i(TAG, "Setting the characteristic (" + characteristic + ") as 'SoundValue' for Dephisit device \"" + bleDevice.getDevice().getName() + "\"");
			this.soundValue = characteristic;
			break;
		default:
			break;
		}
	}
}
