/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * DephisitUtils - Auxiliary implementation methods used for various functionalities.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class DephisitUtils {

	//Code snipped from http://stackoverflow.com/questions/332079/in-java-how-do-i-convert-a-byte-array-to-a-string-of-hex-digits-while-keeping-l/2197650#2197650
    /*public static String toHexString(byte[] bytes) {
        char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2];
        int v;
        for ( int j = 0; j < bytes.length; j++ ) {
            v = bytes[j] & 0xFF;
            hexChars[j*2] = hexArray[v/16];
            hexChars[j*2 + 1] = hexArray[v%16];
        }
        return new String(hexChars);
    }*/
    
    public static String toHexString(byte[] bytes) {
	    if (bytes != null && bytes.length > 0) {
	        final StringBuilder stringBuilder = new StringBuilder(bytes.length);
	        for(byte byteChar : bytes) {
	            stringBuilder.append(String.format("%02X ", byteChar));
	        }
	        return stringBuilder.toString();
	    }
		return null;
    }
}
