/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * LSDeviceListener - Interface to be implemented by clients that use LightSensor devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface LSDeviceListener {

    void onLightValue(LSDeviceI lsDevice, int value);
}

