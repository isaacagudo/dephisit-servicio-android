/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * LADeviceListener - Interface to be implemented by clients that use LightActuator devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface LADeviceListener {

}

