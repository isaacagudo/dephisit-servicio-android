/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.CompoundButton.OnCheckedChangeListener;

/**
 * TSDDeviceActivity - Activity for use TrafficSignalsDetector devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class TSDDeviceActivity extends Activity implements TSDDeviceListener, DephisitCallback {

	private final static String TAG = TSDDeviceActivity.class.getSimpleName();
	
	private DephisitService mBluetoothLeService;
	
	private String deviceId;
	private TSDDeviceI device;

	private boolean mBound;
	
	// Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
        	mBluetoothLeService = ((DephisitService.LocalBinder) service).getService();
        	mBound = true;
        	
        	Log.i(TAG, "BluetoothLeService (API) connected");
        	
        	/*device = (TSDDeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
        	
        	runOnUiThread(new Runnable() {
    			public void run() {
    				((Switch)findViewById(R.id.tsdNotifications)).setEnabled(true);
    			}
    		});*/
        	
        	mBluetoothLeService.scanDephisitDevices(TSDDeviceActivity.this, true, deviceId);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        	mBluetoothLeService = null;
        	mBound = false;
        	
        	Log.i(TAG, "BluetoothLeService (API) disconnected");
        }
    };
	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tsddevice);
		
		Log.d(TAG, "onCreate()");
		
		Intent intent = getIntent();
		
		deviceId = intent.getStringExtra("deviceId");

		Log.d(TAG, "onCreate() - deviceId:" + deviceId);
		
		((TextView)findViewById(R.id.tsdDeviceId)).setText(deviceId);
		
		final Switch sw = (Switch)findViewById(R.id.tsdNotifications);
		sw.setOnCheckedChangeListener(new OnCheckedChangeListener() {
		   @Override
		   public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		 
			   if (sw.isChecked()) {
		    		Toast.makeText(getApplicationContext(), "Notifications enabled", Toast.LENGTH_SHORT).show();
		    		device.enableNotifications(TSDDeviceActivity.this);
		    	} else {
		    		Toast.makeText(getApplicationContext(), "Notifications disabled", Toast.LENGTH_SHORT).show();
		    		device.disableNotifications(TSDDeviceActivity.this);
		    	}
		 
		   }
		});
		sw.setEnabled(false);
		
		// Bind to LocalService
        Intent gattServiceIntent = new Intent(this, DephisitService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
	}
	
	@Override
    protected void onStart() {
    	Log.d(TAG, "onStart()");
    	
        super.onStart();
    }
    
    @Override
    protected void onStop() {
    	Log.d(TAG, "onStop()");
    	
        super.onStop();
    }

    
    @Override
    protected void onDestroy() {
    	Log.d(TAG, "onDestroy()");
    	
        super.onDestroy();
        
        device.disableNotifications(this);
        
        // Unbind from the service
        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }
    }

    public void onNotificationsClicked(View v)
    {
    	Switch sw = (Switch)v;
    	
    	if (sw.isChecked()) {
    		Toast.makeText(getApplicationContext(), "Notifications enabled", Toast.LENGTH_SHORT).show();
    		device.enableNotifications(this);
    	} else {
    		Toast.makeText(getApplicationContext(), "Notifications disabled", Toast.LENGTH_SHORT).show();
    		device.disableNotifications(this);
    	}
    }
	
	@Override
	public void onNewTrafficSignInRange(final TrafficSign type, final float latitude, final float longitude, final int fromDirection, final int toDirection) {
		runOnUiThread(new Runnable() {
			public void run() {
				if (Float.isNaN(latitude) || Float.isNaN(longitude)) {
					((TextView)findViewById(R.id.tsdValue)).setText("Traffic sign position not available\n"+fromDirection+"�->"+toDirection+"�");
				} else {
					((TextView)findViewById(R.id.tsdValue)).setText(latitude+"�\n"+longitude+"�\n"+fromDirection+"�->"+toDirection+"�");
				}
				//Put traffic signal's image
				int resource = 0;
				switch (type) {
					case TRAFFIC_LIGHT_GREEN: resource = R.drawable.green; break;
					case TRAFFIC_LIGHT_YELLOW: resource = R.drawable.yellow; break;
					case TRAFFIC_LIGHT_RED: resource = R.drawable.red; break;
					case TRAFFIC_LIGHT_FLASHING_YELLOW: resource = R.drawable.flashing_yellow; break;
					case TRAFFIC_LIGHT_FLASHING_RED: resource = R.drawable.flashing_red; break;
					case TRAFFIC_LIGHT_OFF: resource = R.drawable.off; break;
					case TRAFFIC_LIGHT_UNKNOWN: resource = R.drawable.unknown; break;
					case STOP_SIGN: resource = R.drawable.stop; break;
					default: break;
				}
				((ImageView)findViewById(R.id.tsdImage)).setBackgroundResource(resource);
			}
		});
	}
	
	@Override
	public void onTrafficSignOutOfRange(final TrafficSign type, final float latitude, final float longitude, final int fromDirection, final int toDirection) {
		runOnUiThread(new Runnable() {
			public void run() {	
				((TextView)findViewById(R.id.tsdValue)).setText("Out Of Range");
				//Put traffic signal's image
				int resource = 0;
				switch (type) {
					case TRAFFIC_LIGHT_GREEN: resource = R.drawable.green; break;
					case TRAFFIC_LIGHT_YELLOW: resource = R.drawable.yellow; break;
					case TRAFFIC_LIGHT_RED: resource = R.drawable.red; break;
					case TRAFFIC_LIGHT_FLASHING_YELLOW: resource = R.drawable.flashing_yellow; break;
					case TRAFFIC_LIGHT_FLASHING_RED: resource = R.drawable.flashing_red; break;
					case TRAFFIC_LIGHT_OFF: resource = R.drawable.off; break;
					case TRAFFIC_LIGHT_UNKNOWN: resource = R.drawable.unknown; break;
					case STOP_SIGN: resource = R.drawable.stop; break;
					default: break;
				}
				((ImageView)findViewById(R.id.tsdImage)).setBackgroundResource(resource);
			}
		});
	}

	@Override
	public void onDephisitDevice(final String deviceId, final boolean available) {
		//if (device.getDeviceId().equals(deviceId)) {
			runOnUiThread(new Runnable() {
				public void run() {
					if (available) {
						device = (TSDDeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
						
						((Switch)findViewById(R.id.tsdNotifications)).setEnabled(true);
						Toast.makeText(getApplicationContext(), "DEPHISIT device available", Toast.LENGTH_SHORT).show();
					} else {
						//device = null;
						
						((Switch)findViewById(R.id.tsdNotifications)).setEnabled(false);
						Toast.makeText(getApplicationContext(), "DEPHISIT device unavailable", Toast.LENGTH_SHORT).show();
					}
				}
			});
		//}
	}

	@Override
	@Deprecated
	public void onScanFinished() {
		// TODO Auto-generated method stub
		
	}
}
