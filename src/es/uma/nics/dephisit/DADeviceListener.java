/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * DADeviceListener - Interface to be implemented by clients that use DisplayActuator devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface DADeviceListener {

}

