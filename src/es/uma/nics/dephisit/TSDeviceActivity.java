/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * TSDeviceActivity - Activity for use TemperatureSensor devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public class TSDeviceActivity extends Activity implements TSDeviceListener, DephisitCallback {

	private final static String TAG = TSDeviceActivity.class.getSimpleName();
	
	private DephisitService mBluetoothLeService;
	
	private String deviceId;
	private TSDeviceI device;

	private boolean mBound;
	
	// Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
        	mBluetoothLeService = ((DephisitService.LocalBinder) service).getService();
        	mBound = true;
        	
        	Log.i(TAG, "BluetoothLeService (API) connected");
        	
        	/*device = (TSDeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
        	
        	runOnUiThread(new Runnable() {
    			public void run() {
    				((Button)findViewById(R.id.tsGetTemp)).setEnabled(true);
    			}
    		});*/
        	
        	mBluetoothLeService.scanDephisitDevices(TSDeviceActivity.this, true, deviceId);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
        	mBluetoothLeService = null;
        	mBound = false;
        	
        	Log.i(TAG, "BluetoothLeService (API) disconnected");
        }
    };
	
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tsdevice);
		
		Log.d(TAG, "onCreate()");
		
		Intent intent = getIntent();
		
		deviceId = intent.getStringExtra("deviceId");

		Log.d(TAG, "onCreate() - deviceId:" + deviceId);
		
		((TextView)findViewById(R.id.tsDeviceId)).setText(deviceId);
		
		Button button = (Button)findViewById(R.id.tsGetTemp);
		button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	device.getTemp(TSDeviceActivity.this);
            }
        });
		button.setEnabled(false);
		
		// Bind to LocalService
        Intent gattServiceIntent = new Intent(this, DephisitService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
	}
	
	@Override
    protected void onStart() {
    	Log.d(TAG, "onStart()");
    	
        super.onStart();
    }
    
    @Override
    protected void onStop() {
    	Log.d(TAG, "onStop()");
    	
        super.onStop();
    }

    
    @Override
    protected void onDestroy() {
    	Log.d(TAG, "onDestroy()");
    	
        super.onDestroy();
        
        // Unbind from the service
        if (mBound) {
            unbindService(mServiceConnection);
            mBound = false;
        }
    }
 
    
	@Override
	public void onTempValue(TSDeviceI tsDevice, final float value) {
		runOnUiThread(new Runnable() {
			public void run() {
				((TextView)findViewById(R.id.tsTempValue)).setText(String.valueOf(value));
			}
		});
	}

	@Override
	public void onDephisitDevice(final String deviceId, final boolean available) {
		//if (device.getDeviceId().equals(deviceId)) {
			runOnUiThread(new Runnable() {
				public void run() {
					if (available) {
						device = (TSDeviceI) mBluetoothLeService.getDephisitDevice(deviceId);
						
						((Button)findViewById(R.id.tsGetTemp)).setEnabled(true);
						Toast.makeText(getApplicationContext(), "DEPHISIT device available", Toast.LENGTH_SHORT).show();
					} else {
						//device = null;
						
						((Button)findViewById(R.id.tsGetTemp)).setEnabled(false);
						Toast.makeText(getApplicationContext(), "DEPHISIT device unavailable", Toast.LENGTH_SHORT).show();
					}
				}
			});
		//}
	}

	@Override
	@Deprecated
	public void onScanFinished() {
		// TODO Auto-generated method stub
		
	}
}
