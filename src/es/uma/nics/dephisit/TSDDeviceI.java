/*
 * Code developed for DEPHISIT project.
 */

package es.uma.nics.dephisit;

/**
 * TSDDeviceI - Interface to be implemented by TrafficSignalsDetector devices.
 * @author Jesus Rodriguez
 * @version 0.1
 */
public interface TSDDeviceI extends DephisitDeviceI {

	boolean areNotificationsEnabled();
	boolean enableNotifications(TSDDeviceListener trafficSignalsDetectorDListener);
	boolean disableNotifications(TSDDeviceListener trafficSignalsDetectorDListener);
}
